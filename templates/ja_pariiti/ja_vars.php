<?php
/**
 * @copyright	Copyright (C) 2005 - 2007 Open Source Matters. All rights reserved.
 * @license		GNU/GPL, see LICENSE.php
 * Joomla! is free software. This version may have been modified pursuant
 * to the GNU General Public License, and as distributed it includes or
 * is derivative of works licensed under the GNU General Public License or
 * other free or open source software licenses.
 * See COPYRIGHT.php for copyright notices and details.
 */

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
include_once (dirname(__FILE__).DS.'/ja_templatetools.php');
$tmpTools = new JA_Tools($this);
$tmpTools->setColorThemes(array('default','red'));
# Auto Collapse Divs Functions ##########
$ja_left = $this->countModules( 'left' );
$ja_right = $this->countModules( 'right' );
if ( $ja_left && $ja_right ) {
  //2 columns on the right
	$divid = '';
} elseif ( ($ja_left || $ja_right) ) {
  //One column without masscol
	$divid = '-c';
} else {
  //No column in right
	$divid = '-f';
}

//Main navigation
$japarams = new JParameter('');
$japarams->set( 'menu_images_align', 'left' );
$japarams->set( 'menutype', 'mainmenu' );
$japarams->set( 'menupath', $tmpTools->templateurl() .'/ja_menus');
$menu = '';
switch ($tmpTools->getParam(JA_TOOL_MENU)) {
	case 2:
		$menu = "CSSmenu";
		include_once( dirname(__FILE__).DS.'ja_menus/'.$menu.'.class.php' );
		break;
	case 3:
		$menu = "Moomenu";
		include_once( dirname(__FILE__).DS.'ja_menus/'.$menu.'.class.php' );
		break;
	case 4:
		$menu = "Slidemenu";
		$japarams->set('minwidth', 80);
		$japarams->set('maxwidth', 180);
		include_once( dirname(__FILE__).DS.'ja_menus/'.$menu.'.class.php' );
		break;
	case 5:
		$menu = "Transmenu";
		include_once( dirname(__FILE__).DS.'ja_menus/'.$menu.'.class.php' );
		break;
	case 6:
		$menu = "DLmenu";
		include_once( dirname(__FILE__).DS.'ja_menus/'.$menu.'.class.php' );
		break;
	default:
		$menu = "Splitmenu";
		include_once( dirname(__FILE__).DS.'ja_menus/'.$menu.'.class.php' );
		break;
}
$menuclass = "JA_$menu";
$jamenu = new $menuclass ($japarams);

$hasSubnav = false;
if (($jamenu->hasSubMenu (1) && $tmpTools->getParam(JA_TOOL_MENU) == 1) || $tmpTools->getParam(JA_TOOL_MENU) == 6) 
	$hasSubnav = true;
//End for main navigation


?>
