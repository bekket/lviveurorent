<?php
/*------------------------------------------------------------------------
# JA Pariiti - June, 2007
# ------------------------------------------------------------------------
# Copyright (C) 2004-2007 J.O.O.M Solutions Co., Ltd. All Rights Reserved.
# @license - Copyrighted Commercial Software
# Author: J.O.O.M Solutions Co., Ltd
# Websites:  http://www.joomlart.com -  http://www.joomlancers.com
-------------------------------------------------------------------------*/

defined( '_JEXEC' ) or die( 'Restricted access' );
defined( 'DS') || define( 'DS', DIRECTORY_SEPARATOR );
include_once (dirname(__FILE__).DS.'ja_vars.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>

<meta name='yandex-verification' content='462826ffc0f3d64b' />

<jdoc:include type="head" />
<?php JHTML::_('behavior.mootools'); ?>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta http-equiv="Content-Style-Type" content="text/css" />
<link rel="stylesheet" href="<?php echo $tmpTools->baseurl(); ?>templates/system/css/system.css" type="text/css" />
<link rel="stylesheet" href="<?php echo $tmpTools->baseurl(); ?>templates/system/css/general.css" type="text/css" />
<link rel="stylesheet" href="<?php echo $tmpTools->templateurl();?>/css/editor.css" type="text/css" />
<link href="<?php echo $tmpTools->templateurl();?>/css/template_css.css" rel="stylesheet" type="text/css" />
<script src="http://cdn.connect.mail.ru/js/loader.js" type="text/javascript"></script>
<script src="http://stg.odnoklassniki.ru/share/odkl_share.js" type="text/javascript"></script>
<!-- Put this script tag to the <head> of your page -->
<script type="text/javascript" src="http://userapi.com/js/api/openapi.js?23"></script>

<script type="text/javascript">
  VK.init({apiId: API_ID, onlyWidgets: true});
</script>


<script language="javascript" type="text/javascript" src="<?php echo $tmpTools->templateurl();?>/scripts/ja.script.js"></script>

<?php $tmpTools->genMenuHead(); ?>

<!--[if lte IE 6]>
<style type="text/css">
.clearfix {	height: 1%;}
</style>
<![endif]-->

<!--[if gte IE 7.0]>
<style type="text/css">
.clearfix {	display: inline-block;}
</style>
<![endif]-->

<link href="<?php echo $tmpTools->templateurl();?>/css/colors/<?php echo $tmpTools->getParam(JA_TOOL_COLOR); ?>.css" rel="stylesheet" type="text/css" />

<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-12404951-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
</head>

<body id="bd" class="<?php echo $tmpTools->getParam(JA_TOOL_SCREEN)." fs".$tmpTools->getParam(JA_TOOL_FONT);?>">

<!-- Yandex.Metrika counter --><div style="display:none;"><script type="text/javascript">(function(w, c) { (w[c] = w[c] || []).push(function() { try { w.yaCounter4096273 = new Ya.Metrika({id:4096273}); } catch(e) { } }); })(window, "yandex_metrika_callbacks");</script></div><script src="//mc.yandex.ru/metrika/watch.js" type="text/javascript" defer="defer"></script><noscript><div><img src="//mc.yandex.ru/watch/4096273" style="position:absolute; left:-9999px;" alt="" /></div></noscript><!-- /Yandex.Metrika counter -->

<ul class="accessibility">
	<li><a href="<?php echo $tmpTools->getCurrentURL();?>#ja-content" title="Skip to content">Skip to content</a></li>
	<li><a href="<?php echo $tmpTools->getCurrentURL();?>#ja-col1" title="">Skip to 1st column</a></li>
	<li><a href="<?php echo $tmpTools->getCurrentURL();?>#ja-col2" title="">Skip to 2nd column</a></li>
</ul>

<a name="Top" id="Top"></a>

<div id="ja-upwrap" class="clearfix">

	<div id="ja-up">
	<?php if ($tmpTools->getParam(JA_TOOL_USER)) { ?>
	<div id="ja-usertools">
		<?php $tmpTools->genToolMenu($tmpTools->getParam(JA_TOOL_USER) & 7,'gif'); /*screen tool*/ ?>
	</div><div class="clr"></div>
	<?php } ?>
	
	<?php if ( $this->countModules('user4') ) {	?>
	<div id="ja-search">
		<jdoc:include type="modules" name="user4" style="raw" />
	</div>
	<?php } ?>

	</div>
	
</div>

<div id="ja-containerwrap<?php echo $divid ?>">
<div id="ja-container">

<!-- BEGIN: HEADER -->
<div id="ja-header" class="clearfix">
	<h1><a href="index.php"><?php echo $tmpTools->sitename()?></a></h1>
</div>
<!-- END: HEADER -->

<!-- BEGIN: SUB-HEADER -->
<?php if ( $this->countModules('slideshow')  && $this->countModules('top') ) { ?>
<div id="ja-sh">
  <div class="ja-innerpad clearfix">
  
  <?php if ($this->countModules('slideshow')) { ?>
  <div id="ja-slideshow">
  	<jdoc:include type="modules" name="slideshow" style="xhtml" />
  </div>
  <?php } ?>
  
  <?php if ($this->countModules('top')) { ?>
    <div id="ja-shcontent">
      <div class="innerpad">
	      <jdoc:include type="modules" name="top" style="xhtml" />
      </div>
    </div>
  <?php } ?>
  
  </div>
</div>
<?php } ?>
<!-- END: SUB-HEADER -->

<!-- BEGIN: MAIN NAVIGATION -->
<div id="ja-mainnav-wrap">
  <div id="ja-mainnav">
    <div class="ja-mainnav-inner clearfix">
  	<?php
  		switch ($tmpTools->getParam(JA_TOOL_MENU)) {
  			case 6: 
  				$jamenu->genMenu (0);
  				break;
  			case 2:
  				echo "<div class=\"sfmenu-inner\">";
  					$jamenu->genMenu (0);
  				echo "</div>";
  				break;
  			case 5:
  				echo "<div class=\"transmenu-inner\">";
  					$jamenu->genMenu (0);
  				echo "</div>";
  				break;
    		case 1: 
    			$jamenu->genMenu (0);
    			break;
  		}
  	?>
  	</div>
  </div>
</div>
<?php if ($hasSubnav && $tmpTools->getParam(JA_TOOL_MENU) == 5) { ?>
<div id="ja-subnavwrap">
  <div id="ja-subnav" class="clearfix">
  	<?php $jamenu->genMenu (1,1);	?>
  </div>
</div>
<?php } ?>
<?php if ($tmpTools->getParam(JA_TOOL_MENU) == 6) { ?>
<div id="ja-subnavwrap">
  <div id="ja-subnav" class="clearfix" style="height:33px;">
  	<?php $jamenu->genMenu (1,1);	?>
  </div>
</div>
<?php } ?>
<!-- END: MAIN NAVIGATION -->

<?php
$spotlight = array ('user1','user2','user6');
$topsl = $tmpTools->calSpotlight ($spotlight);
if( $topsl ) {
?>
<!-- BEGIN: TOP SPOTLIGHT -->
<div id="ja-topslwrap">
	<div id="ja-topsl" class="clearfix">
	
		<?php if( $this->countModules('user1') ) {?>
	  <div class="ja-box<?php echo $topsl['user1']['class']; ?>" style="width: <?php echo $topsl['user1']['width']; ?>;">
	     <jdoc:include type="modules" name="user1" style="xhtml" />	  	
	  </div>
	  <?php } ?>

	  <?php if( $this->countModules('user2') ) {?>
	  <div class="ja-box<?php echo $topsl['user2']['class']; ?>" style="width: <?php echo $topsl['user2']['width']; ?>;">
	     <jdoc:include type="modules" name="user2" style="xhtml" />
	  </div>
	  <?php } ?>

	  <?php if( $this->countModules('user6') ) {?>
	  <div class="ja-box<?php echo $topsl['user6']['class']; ?>" style="width: <?php echo $topsl['user6']['width']; ?>;">
	     <jdoc:include type="modules" name="user6" style="xhtml" />
	  </div>
	  <?php } ?>

	</div>
</div>
<!-- END: TOP SPOTLIGHT -->
<?php } ?>

<div id="ja-contentwrap" class="clearfix">
<!-- BEGIN: CONTENT -->
<div id="ja-content">
	<div class="innerpad">

		<div id="ja-current-content">
		<?php if ($tmpTools->isFrontPage()) {?>
		<div id="ja-pathway">
			<jdoc:include type="module" name="breadcrumbs" />
		</div><div class="clr"></div>
		<?php } ?>
	<jdoc:include type="message" />
    <jdoc:include type="component" />
    <?php if ( $this->countModules('banner') ) { ?>
    <br />
		<div class="ja-banner">
			<jdoc:include type="modules" name="banner" style="raw" />		
		</div>
		<?php } ?>
    </div>

	</div>
</div>
<!-- END: CONTENT -->

<?php if ($ja_left || $ja_right ) { ?>
<!-- BEGIN: COLUMNS -->
<div id="ja-colwrap">

	<?php if ($ja_left) { ?>
	<div id="ja-col1">
		<div><div class="clearfix">
			<jdoc:include type="modules" name="left" style="xhtml" />		
		</div></div>
	</div>
	<?php } ?>

	<?php if ($ja_right) { ?>
	<div id="ja-col2">
		<jdoc:include type="modules" name="right" style="xhtml" />		
	</div>
	<?php } ?>

</div><br />
<!-- END: COLUMNS -->
<?php } ?>
</div>

<?php
$spotlight = array ('user7','user8','user9','user10');
$botsl = $tmpTools->calSpotlight ($spotlight, 99);
if( $botsl ) {
?>
<!-- BEGIN: BOTTOM SPOTLIGHT -->
<div id="ja-botslwrap">
	<div id="ja-botsl" class="clearfix">
	
	  <?php if( $this->countModules('user7') ) {?>
	  <div class="ja-box<?php echo $botsl['user7']['class']; ?>" style="width: <?php echo $botsl['user7']['width']; ?>;">
	  	<jdoc:include type="modules" name="user7" style="xhtml" />	    
	  </div>
	  <?php } ?>

	  <?php if( $this->countModules('user8') ) {?>
	  <div class="ja-box<?php echo $botsl['user8']['class']; ?>" style="width: <?php echo $botsl['user8']['width']; ?>;">
	    <jdoc:include type="modules" name="user8" style="xhtml" />
	  </div>
	  <?php } ?>

	  <?php if( $this->countModules('user9') ) {?>
	  <div class="ja-box<?php echo $botsl['user9']['class']; ?>" style="width: <?php echo $botsl['user9']['width']; ?>;">
	    <jdoc:include type="modules" name="user9" style="xhtml" />
	  </div>
	  <?php } ?>
	  
	  <?php if( $this->countModules('user10') ) {?>
	  <div class="ja-box<?php echo $botsl['user10']['class']; ?>" style="width: <?php echo $botsl['user10']['width']; ?>;">
	    <jdoc:include type="modules" name="user10" style="xhtml" />
	  </div>
	  <?php } ?>

	</div>
</div>
<!-- END: BOTTOM SPOTLIGHT -->
<?php } ?>

<!-- BEGIN: FOOTER -->
<div id="ja-footer" class="clearfix">
  <small>Copyright © 2005 - 2011 Подобова оренда квартир у Львові.</small>
  	<jdoc:include type="modules" name="user3" style="row" />

<!--bigmir)net TOP 100-->
<script type="text/javascript" language="javascript"><!--
function BM_Draw(oBM_STAT){
document.write('<table cellpadding="0" cellspacing="0" border="0" style="display:inline;margin-right:4px;"><tr><td><div style="font-family:Tahoma;font-size:10px;padding:0px;margin:0px;"><div style="width:7px;float:left;background:url(\'http://i.bigmir.net/cnt/samples/default/b57_left.gif\');height:17px;padding-top:2px;background-repeat:no-repeat;"></div><div style="float:left;background:url(\'http://i.bigmir.net/cnt/samples/default/b57_center.gif\');text-align:left;height:17px;padding-top:2px;background-repeat:repeat-x;"><a href="http://www.bigmir.net/" target="_blank" style="color:#0000ab;text-decoration:none;">bigmir<span style="color:#ff0000;">)</span>net</a>&nbsp;&nbsp;<span style="color:#71b27e;">хиты</span>&nbsp;<span style="color:#12351d;font:10px Tahoma;">'+oBM_STAT.hits+'</span>&nbsp;<span style="color:#71b27e;">хосты</span>&nbsp;<span style="color:#12351d;font:10px Tahoma;">'+oBM_STAT.hosts+'</span></div><div style="width:7px;float: left;background:url(\'http://i.bigmir.net/cnt/samples/default/b57_right.gif\');height:17px;padding-top:2px;background-repeat:no-repeat;"></div></div></td></tr></table>');
}
//-->
</script>
<script type="text/javascript" language="javascript"><!--
bmN=navigator,bmD=document,bmD.cookie='b=b',i=0,bs=[],bm={o:1,v:16815023,s:16815023,t:0,c:bmD.cookie?1:0,n:Math.round((Math.random()* 1000000)),w:0};
for(var f=self;f!=f.parent;f=f.parent)bm.w++;
try{if(bmN.plugins&&bmN.mimeTypes.length&&(x=bmN.plugins['Shockwave Flash']))bm.m=parseInt(x.description.replace(/([a-zA-Z]|\s)+/,''));
else for(var f=3;f<20;f++)if(eval('new ActiveXObject("ShockwaveFlash.ShockwaveFlash.'+f+'")'))bm.m=f}catch(e){;}
try{bm.y=bmN.javaEnabled()?1:0}catch(e){;}
try{bmS=screen;bm.v^=bm.d=bmS.colorDepth||bmS.pixelDepth;bm.v^=bm.r=bmS.width}catch(e){;}
r=bmD.referrer.slice(7);if(r&&r.split('/')[0]!=window.location.host){bm.f=escape(r);bm.v^=r.length}
bm.v^=window.location.href.length;for(var x in bm) if(/^[ovstcnwmydrf]$/.test(x)) bs[i++]=x+bm[x];
bmD.write('<sc'+'ript type="text/javascript" language="javascript" src="http://c.bigmir.net/?'+bs.join('&')+'"></sc'+'ript>');
//-->
</script>
<noscript>
<a href="http://www.bigmir.net/" target="_blank"><img src="http://c.bigmir.net/?v16815023&s16815023&t2" width="88" height="31" alt="bigmir)net TOP 100" title="bigmir)net TOP 100" border="0" /></a>
</noscript>
<!--bigmir)net TOP 100-->
	
</div> 
<!-- END: FOOTER -->

</div>
</div>

<jdoc:include type="modules" name="debug" style="raw" />
</body>

</html>