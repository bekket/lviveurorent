
mooSlideMenu = new Class ({
	initialize: function(ul, options){
		this.options = Object.extend({
			mode: 'vir',  //vir, hor
			anim: 'slide' //slide, expend
		}, options || {});
		if(!ul || !ul.getChildren().length) return;
		ul.getChildren().each(function(el){
			if(el.getChildren().length){
				var elu = null;
				if(el.getElementsByTagName('DIV').length){
					elu = $(el.getElementsByTagName('DIV')[0]);
				}
				if (elu)
				{
			
					//Get ul
					if(elu.getChildren().length) {
						var ul = elu.getChildren()[0];
						if (this.options.mode=='vir')
						{
							el.f = 'height';
							el.openx = ul.offsetHeight;
							elu.setStyle('width', ul.offsetWidth);
						}else{
							el.f = 'width';
							el.openx = ul.offsetWidth;
							elu.setStyles({
								'height': ul.offsetHeight,
								'left': el.offsetWidth,
								'top': 0
							});
						}

						if (this.options.anim=='slide')
						{
							ul.setStyles({
								'bottom': 0,
								'right': 0
							});
						}else{
							ul.setStyles({
								'top': 0,
								'left': 0
							});
						}
					} else {
						return;
					}
					
					updateStatus=function () {
						if (this.fx.now == this.openx)
						{
							this.elu.setStyle ('overflow', 'visible');		
						}
					}

					el.fx = new Fx.Style(elu, el.f, {duration: 300, wait: false, onComplete: updateStatus.bind(el)});
					el.elu = elu;
					el.addEvent('mouseenter',function(e){
						el.open = true;
						this.elu.setStyle ('overflow', 'hidden');
						e = new Event(e);
						this.fx.start (this.openx);
						e.stop();
					})
					el.addEvent('mouseleave',function(e){
						el.open = false;
						this.elu.setStyle ('overflow', 'hidden');
						this.fx.start (0);
					})
					new mooSlideMenu(elu.getChildren()[0], $extend(options, {mode:'hor'}));
				}
			}

			el.addEvent('mouseenter',function(e){
					this.className+='sfhover';
			});
			el.addEvent('mouseleave',function(e){
					this.className=this.className.replace(new RegExp("sfhover\\b"), "");
			});

		},this);
	}
});

Window.onDomReady(function() {new mooSlideMenu($('ja-cssmenu'), {mode:'vir', anim:'slide'})});
