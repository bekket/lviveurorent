var JASplit2Menu = new Class({
	
	initialize: function(el, options){
		this.options = Object.extend({
			minwidth: 100,
			maxwidth: 180
		}, options || {});
		
		items = el.getChildren();
		if (items.length < 2) return;

		this.options.normalwidth = ((items.length-1)*this.options.minwidth + this.options.maxwidth) / items.length;
		var fx = new Fx.Elements(items, {wait: false, duration: 200, transition: Fx.Transitions.quadOut});
		items.each(function(item, i){
			item.setStyle('width', this.options.normalwidth);

			item.addEvent('mouseenter', function(e){
				var obj = {};
				obj[i] = {
					'width': [item.getStyle('width').toInt(), this.options.maxwidth]
				};
				items.each(function(other, j){
					if (other != item){
						var w = other.getStyle('width').toInt();
						if (w != this.options.minwidth) obj[j] = {'width': [w, this.options.minwidth]};
					}
				}.bind(this));
				fx.start(obj);
			}.bind(this));
		}.bind(this));
		
		el.addEvent('mouseleave', function(e){
			var obj = {};
			items.each(function(other, j){
				obj[j] = {'width': [other.getStyle('width').toInt(), this.options.normalwidth]};
			}.bind(this));
			fx.start(obj);
		}.bind(this));
	}
	
});