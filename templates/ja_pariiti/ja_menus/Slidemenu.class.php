<?php
defined( '_VALID_MOS' ) or defined('_JEXEC') or die('Restricted access');
if (!defined ('_JA_SLIDE_MENU_CLASS')) {
	define ('_JA_SLIDE_MENU_CLASS', 1);
	require_once (dirname(__FILE__).DS."Base.class.php");

	class JA_Slidemenu extends JA_Base{
		function JA_Slidemenu (&$params) {
			$params->set('menu_images', 1);
			$params->set('menu_background', 1);
			parent::JA_Base($params);

			//To show sub menu on a separated place
			$this->showSeparatedSub = true;
		}

		function beginMenu($startlevel=0, $endlevel = 10){
			if ($startlevel == 0) {
				echo "<div id=\"ja-slidemenu\" class=\"mainlevel clearfix\">\n";
			} else {
				echo "<div class=\"sublevel\">\n";
			}
		}

		function endMenu($startlevel=0, $endlevel = 10){
			echo "\n</div>";
			$options = "";
			if($this->getParam('minwidth')) $options .= 'minwidth:'.$this->getParam('minwidth');
			if($this->getParam('maxwidth')) $options .= ',maxwidth:'.$this->getParam('maxwidth');
			$options = "{".$options."}";
			?>
			<script type="text/javascript">
				window.addEvent('domready', function(){new JASplit2Menu($E('#ja-slidemenu ul'),<?php echo $options;?>);});
			</script>
			<?php
		}

		function genMenu($startlevel=0, $endlevel = 10){
			if ($startlevel == 0) parent::genMenu(0,0);
			else parent::genMenu($startlevel, $endlevel);
		}

		function beginMenuItem($mitem=null, $level = 0, $pos = ''){
			$active = $this->genClass ($mitem, $level, $pos);
			echo "<li ".$active.">";
		}

		function genMenuHead () {
			?>
			<link href="<?php echo $this->getParam('menupath'); ?>/ja_slidemenu/ja.slidemenu.css" rel="stylesheet" type="text/css" />
			<script src="<?php echo $this->getParam('menupath'); ?>/ja_slidemenu/ja.slidemenu.js" language="javascript" type="text/javascript"></script>
			<?php
		}
	}
}
?>
