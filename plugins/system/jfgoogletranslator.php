<?php
/**
 * @version		$Id: jfgoogletranslator.php 10715 2010-03-05 12:50:14Z  $
 * @package		Joomla
 * @subpackage	System
 * @copyright	Copyright (C) 2005 - 2009 Viviendo Linux. All rights reserved.
 * @license		GNU/GPL, see LICENSE.php
 * Joomla! is free software. This version may have been modified pursuant
 * to the GNU General Public License, and as distributed it includes or
 * is derivative of works licensed under the GNU General Public License or
 * other free or open source software licenses.
 * See COPYRIGHT.php for copyright notices and details.
 * 
 * Known errors:
 * 
 * jfrouter must not do 302 redirects, or the introtext and fulltext will have an ugly 404 error
 */

// Check to ensure this file is included in Joomla!
defined( '_JEXEC' ) or die( 'Restricted access' );

jimport( 'joomla.plugin.plugin' );
jimport( 'joomla.installer.installer' );

define('USER_AGENT','Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.2.1) Gecko/20021204');

define('LANGS_PATTERN', 'it|ko|zh-CN|zh-TW|pt|en|de|fr|es|ja|ar|ru|el|nl|zh|zt|no|bg|cs|hr|da|fi|hi|pl|ro|sv|ca|tl|iw|id|lv|lt|sr|sk|sl|uk|vi|sq|et|gl|mt|th|tr|hu');
define('LANGS_PATTERN_WITH_SLASHES', '/it/|/ko/|/zh-CN/|/zh-TW/|/pt/|/en/|/de/|/fr/|/es/|/ja/|/ar/|/ru/|/el/|/nl/|/zh/|/zt/|/no/|/bg/|/cs/|/hr/|/da/|/fi/|/hi/|/pl/|/ro/|/sv/|/ca/|/tl/|/iw/|/id/|/lv/|/lt/|/sr/|/sk/|/sl/|/uk/|/vi/|/sq/|/et/|/gl/|/mt/|/th/|/tr/|/hu/');
define('LANGS_PATTERN_WITHOUT_FINAL_SLASH', '/it|/ko|/zh-CN|/zh-TW|/pt|/en|/de|/fr|/es|/ja|/ar|/ru|/el|/nl|/zh|/zt|/no|/bg|/cs|/hr|/da|/fi|/hi|/pl|/ro|/sv|/ca|/tl|/iw|/id|/lv|/lt|/sr|/sk|/sl|/uk|/vi|/sq|/et|/gl|/mt|/th|/tr|/hu');
define('CHARSET_CONVERSIONS','ISO-8859-8-I|ISO-8859-8;');
define('LANGS_TO_UTF8','en|es|pt|it|fr|de');
define('LANGS_TO_HTMLDECODE','ru|he|zh'); // Here are langs that need to be html-entity decoded
define('TRANSLATION_ENGINE', 'google');
define('HARD_CLEAN', true);

$gltr_ua = array(
"Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10.4; en-US; rv:1.9b5)",
"Mozilla/5.0 (Macintosh; U; PPC Mac OS X Mach-O; en-US; rv:1.0.1)",
"Mozilla/5.0 (Macintosh; U; PPC Mac OS X Mach-O; en-US; rv:1.0.1)",
"Mozilla/5.0 (Macintosh; U; PPC Mac OS X; en-US)",
"Mozilla/5.0 (Macintosh; U; PPC Mac OS X; en-us)",
"Mozilla/5.0 (Macintosh; U; PPC; en-US; rv:0.9.2)",
"Mozilla/5.0 (Windows; U; Win98; en-US; rv:0.9.2)",
"Mozilla/5.0 (Windows; U; Win98; en-US; rv:x.xx)",
"Mozilla/5.0 (Windows; U; Win9x; en; Stable)",
"Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.0.5)",
"Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:x.x.x)",
"Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:x.xx)",
"Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:x.xxx)",
"Mozilla/5.0 (Windows; U; Windows NT 6.0; en-US; rv:1.9b5)",
"Mozilla/5.0 (Windows; U;XMPP Tiscali Communicator v.10.0.1; Windows NT 5.1; it; rv:1.8.1.3)",
"Mozilla/5.0 (X11; Linux i686; U;rv: 1.7.13)",
"Mozilla/5.0 (X11; U; Linux 2.4.2-2 i586; en-US; m18)",
"Mozilla/5.0 (X11; U; Linux i686; en-GB; rv:1.7.6)",
"Mozilla/5.0 (X11; U; Linux i686; en-US; Nautilus/1.0Final)",
"Mozilla/5.0 (X11; U; Linux i686; en-US; rv:0.9.3)",
"Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.2b)",
"Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.6)",
"Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.8.0.7)",
"Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.8.1)",
"Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.8.1.1)",
"Mozilla/5.0 (X11; U; Linux x86_64; en-US; rv:1.9a8)",
);



$gltr_engine = $gltr_available_engines[TRANSLATION_ENGINE];

/**
 * JoomFish Google Translator Content Plugin
 *
 * @package		Joomla
 * @subpackage	Content
 * @since 		1.5
 */
class plgSystemJFGoogleTranslator extends JPlugin
{
    public $opts = array("text" => "", "language_pair" => "es|en");
    public $out = "";
	public $well_known_extensions =  
    array('swf','gif','jpg','jpeg','bmp','gz','zip','rar','tar','png','xls',
    'doc','ppt','tiff','avi','mpeg','mp3','mov','mp4','c','sh','bat');

	/**
	 * Constructor
	 *
	 * For php4 compatability we must not use the __constructor as a constructor for plugins
	 * because func_get_args ( void ) returns a copy of all passed arguments NOT references.
	 * This causes problems with cross-referencing necessary for the observer design pattern.
	 *
	 * @param object $subject The object to observe
	 * @param object $params  The object that holds the plugin parameters
	 * @since 1.5
	 */
	function plgSystemJFGoogleTranslator( &$subject, $params )
	{
		parent::__construct( $subject, $params );
		
		// Check if it is already installed (the db table), if not, then create it
	}

	/**
	 *
	 * Method is called by the view
	 *
	 * @param 	object		The article object.  Note $article->text is also available
	 * @param 	object		The article params
	 * @param 	int			The 'page' number
	 */
	function onPrepareContent( &$article, &$params, $limitstart )
	{
		global $mainframe;
	}

	/**
	 *
	 * Method is called by the view and the results are imploded and displayed in a placeholder
	 *
	 * @param 	object		The article object.  Note $article->text is also available
	 * @param 	object		The article params
	 * @param 	int			The 'page' number
	 * @return	string
	 */
	function onAfterDisplayTitle( &$article, &$params, $limitstart )
	{
		global $mainframe;

		return '';
	}

	/**
	 *
	 * Method is called by the view and the results are imploded and displayed in a placeholder
	 *
	 * @param 	object		The article object.  Note $article->text is also available
	 * @param 	object		The article params
	 * @param 	int			The 'page' number
	 * @return	string
	 */
	function onBeforeDisplayContent( &$article, &$params, $limitstart )
	{
		global $mainframe;

		return '';
	}

	/**
	 *
	 * Method is called by the view and the results are imploded and displayed in a placeholder
	 *
	 * @param 	object		The article object.  Note $article->text is also available
	 * @param 	object		The article params
	 * @param 	int			The 'page' number
	 * @return	string
	 */
	function onAfterDisplayContent( &$article, &$params, $limitstart )
	{
		global $mainframe;
		if($this->params->get('enabled')==1) {
			//self::install();
			if($this->params->get('debug')==1) $this->debug=1; else $this->debug=0;
			if($this->params->get('autopublish')==1) $this->autopublish=1; else $this->autopublish=0;
			if(intval($this->params->get('delaytime'))>0) $this->delaytime=intval($this->params->get('delaytime'))*60; else $this->delaytime=300; // Defaults to 5 minutes
			if(trim($this->params->get('onlyfromthis'))!="") $this->onlyfromthis=trim($this->params->get('onlyfromthis')); else $this->onlyfromthis=""; 
			if($this->params->get('onlyonbots')==0 || eregi(".*(googlebot|slurp).*",$_SERVER[HTTP_USER_AGENT]) || isset($_REQUEST[jftranslator])) { // Run translation only on bots visits
				$this->onlyonbots=$this->params->get('onlyonbots');
				self::translation($article);
			}
		}
		return '';
	}

	/**
	 *
	 * Method is called right before content is saved into the database.
	 * Article object is passed by reference, so any changes will be saved!
	 * NOTE:  Returning false will abort the save with an error.
	 * 	You can set the error by calling $article->setError($message)
	 *
	 * @param 	object		A JTableContent object
	 * @param 	bool		If the content is just about to be created
	 * @return	bool		If false, abort the save
	 */
	function onBeforeContentSave( &$article, $isNew )
	{
		global $mainframe;

		return true;
	}

	/**
	 * Article is passed by reference, but after the save, so no changes will be saved.
	 * Method is called right after the content is saved
	 *
	 *
	 * @param 	object		A JTableContent object
	 * @param 	bool		If the content is just about to be created
	 * @return	void
	 */
	function onAfterContentSave( &$article, $isNew )
	{
		global $mainframe,$params;
		return true;
	}
	
	/**
	 * This function installs (creates) translation table in SQL database
	 * 
	 * @return bool
	 */
	function install() {
		$registry =& JFactory::getConfig();
		
		// Find language without loading strings (user language)       
		$locale = $registry->getValue('config.language');
		
		// Attention - we need to access the site default values
		// #12943 explains that a user might overwrite the orignial settings based on his own profile
		$langparams = JComponentHelper::getParams('com_languages');
		$registry->setValue("config.defaultlang",$langparams->get("site"));
		$locale = $registry->getValue('config.defaultlang'); // default (site) language

		// get instance of JoomFishManager to obtain active language list and config values
		$jfm =&  JoomFishManager::getInstance();

		/*
		$client_lang = '';
		$lang_known = false;
		$jfcookie = JRequest::getVar('jfcookie', null ,"COOKIE");
		if (isset($jfcookie["lang"]) && $jfcookie["lang"] != "") {
				$client_lang = $jfcookie["lang"];
				$lang_known = true;
		}

		$uri =& JURI::getInstance();
		if ($requestlang = JRequest::getVar('lang', null ,"REQUEST")){
				if( $requestlang != '' ) {
						$client_lang = $requestlang;
						$lang_known = true;
				}
		}
		*/
		$activeLanguages = $jfm->getActiveLanguages();
		// falta indicar collate
		foreach($activeLanguages as $language=>$langvalues) {
			if($language!=$locale && $langvalues->active==1) {
			}
		}
		return true;
	}
	
	function getToken() {
		$registry =& JFactory::getConfig();
		return md5(intval(time()/60).$registry->getValue('config.secret'));
	}
	
	/**
	 * This function replaces some group of tags and mambots with an special tag, to avoid google to try to 
	 * translate them. I i let this tags and mambots pass, then google makes a horrible thing with them
	 * I replace them with special tags, and after translation i will replace them again with the original
	 * text.
	 * We need to replace mambots and some other special blocks before show it to translator, blocks like
	 * <div class="rj_insertcode">.....</div>
	 * and
	 * {pluginname ...}....{/pluginname}
	 * and
	 * {pluginname ...}
	 * 
	 * Also, we need to switch images, because google translator does bad translation on them, making them 
	 * unavailable sometimes
	 * 
	 * @param	string	A string with the text to search and replace
	 * @param	string  A string indicating where to save the original tags. If null, then i return an array
	 * @return	string or an array, if array, then first is the new text and second is an array with the original tags
	 */
	function switchTags($txt, $saveto="") {
		global $tagcount;
		//$tagcount=0;
		$tagcount=intval($tagcount);
		while(($inipos=strpos($txt,'<div class="rj_insertcode">'))>0) {
			$endpos=strpos($txt,'</div>',$inipos);
			$endpos=strpos($txt,'</div>',$endpos);
			$tags["jfgtrl_$tagcount"]=substr($txt,$inipos,$endpos+14-$inipos);
			$txt=substr($txt,0,$inipos)."<jfgtrl_$tagcount>".substr($txt,$endpos+14);
			$tagcount++;
		}
		$pattern="/\{([a-zA-Z]+)\s[^\}]*\}/"; // Replace plugins of the style {something otherdata}
		preg_match_all($pattern, $txt, $matches);
		//echo "<xmp>"; print_r($matches); echo("</xmp>");
		foreach($matches[0] as $k=>$match) {
			$txt=str_replace($match, "<jfgtrl_$tagcount>", $txt);
			$tags["jfgtrl_$tagcount"]=$match;
			$tagcount++;
			if(strstr($txt,"{/".$matches[1][$k]."}")) {
				$txt=str_replace("{/".$matches[1][$k]."}","<jfgtrl_$tagcount>",$txt);
				$tags["jfgtrl_$tagcount"]="{/".$matches[1][$k]."}";
				$tagcount++;
			}
		}
		$pattern="/\{[\/]{0,1}([a-zA-Z]+)\}/"; // Replace plugins of the style {something} and {/something}
		preg_match_all($pattern, $txt, $matches);
		//echo "<xmp>"; print_r($matches); echo("</xmp>");
		foreach($matches[0] as $k=>$match) {
			$txt=str_replace($match, "<jfgtrl_$tagcount>", $txt);
			$tags["jfgtrl_$tagcount"]=$match;
			$tagcount++;
			if(strstr($txt,"{/".$matches[1][$k]."}")) {
				$txt=str_replace("{/".$matches[1][$k]."}","<jfgtrl_$tagcount>",$txt);
				$tags["jfgtrl_$tagcount"]="{/".$matches[1][$k]."}";
				$tagcount++;
			}
		}
		// Replace Images with tags
		//$pattern="/\<(img|a|\/a)\s[^\>]+\>/";
		$pattern="/\<(img)\s[^\>]+\>/i";
		preg_match_all($pattern, $txt, $matches);
		//echo "<xmp>"; print_r($matches); echo("</xmp>");
		foreach($matches[0] as $k=>$match) {
			$txt=str_replace($match, "<jfgtrl_$tagcount>", $txt);
			$tags["jfgtrl_$tagcount"]=$match;
			$tagcount++;
			if(strstr($txt,"{/".$matches[1][$k]."}")) {
				$txt=str_replace("{/".$matches[1][$k]."}","<jfgtrl_$tagcount>",$txt);
				$tags["jfgtrl_$tagcount"]="{/".$matches[1][$k]."}";
				$tagcount++;
			}
		}

		if($saveto=="") return array($txt, $tags);
		if(($fp=fopen($saveto,"w"))==true) {
			fwrite($fp,serialize($tags));
			fclose($fp);
		}
		return $txt;
	}
	/**
	 * This function translates the content to all languages configured in JoomFish
	 * To avoid ip ban from Google, it does it with a delay of 5 minutes between each translation
	 * and maintains a local database of phrases to avoid requesting same translation multiple
	 * times, and speeding up the translation process. First fetch one-row translations, like title, alias, metakey and metadesc
	 * then fetch multirow translations like introtext and fulltext.
	 * 
	 * For first, I use method 1, that translate only a row
	 * For second, I use method 2, that translate blocks
	 * 
	 * Once an article is completly translated, it is inserted in JoomFish database
	 * 
	 * @param	object		A JTableContent object
	 * @return	bool
	 */
	function translation(&$article) {
		jimport('joomla.filesystem.path');
		
		$jftarticle=clone $article;
//        $safepath=JPATH_SITE.DS.JPath::clean($this->params->get('imagespath'));
		$basepath=JPATH_SITE.DS.'cache'.DS.'jfgoogletranslator';
		@mkdir($basepath,0755);
		$this->basepath=$basepath;

		if($_REQUEST[jftest]==1) {
			$fp=fopen($basepath.DS."traduccion-full.txt","r");
			$fullhtml=fread($fp,filesize($basepath.DS."traduccion-full.txt"));
			fclose($fp);
			$cleanhtml=$this->gltr_clean_translated_page($fullhtml, $srclang);
			$fp=fopen($this->basepath."/traduccion-full.txt","w");
			fwrite($fp,$fullhtml);
			fclose($fp);
			$fp=fopen($this->basepath."/traduccion-clean.txt","w");
			fwrite($fp,$cleanhtml);
			fclose($fp);
			echo "<xmp>$cleanhtml</xmp>";
			exit;
		}
		
		$registry =& JFactory::getConfig();
		$langparams = JComponentHelper::getParams('com_languages');
		$registry->setValue("config.defaultlang",$langparams->get("site"));
		$locale = $registry->getValue('config.defaultlang'); // default language
		$jfm =&  JoomFishManager::getInstance();
		$activeLanguages = $jfm->getActiveLanguages();
		list($srclang,)=split("\-",$locale); // Set default (source) language
		
		// Look for active languages, if more than 1, then choose one random, if only 1 then choose it
		foreach($activeLanguages as $language=>$langvalues) {
			list($simplelang,)=split("\-",$language);
			if($language!=$locale && $langvalues->active==1) {
				@mkdir($basepath.DS.$language,0755);
				// This should select default language first, and then others... don't work right now
				if(isset($_REQUEST[lang])) {
					if(($_REQUEST[lang]==$simplelang || $_REQUEST[lang]==$language) && $_REQUEST[lang]!=$srclang)
						$defaultdstlang=array($simplelang, $language, $langvalues->id);
				}
				$dstlangs[]=array($simplelang, $language, $langvalues->id);
			}
		}
		$langchoosed=rand(0,count($dstlangs)-1);
		list($dstlang,$dstlangfull,$dstlangid)=$dstlangs[$langchoosed];
		if(isset($defaultdstlang)) list($dstlang,$dstlangfull,$dstlangid)=$defaultdstlang;

    $db     =& JFactory::getDBO();
    
    
    if($_REQUEST[extraoption]=="clean") {
			$sSQL="SELECT reference_id FROM #__jf_content WHERE reference_table='content' AND reference_field = 'title' AND value LIKE '%</%' LIMIT 1000";
			$db->setQuery($sSQL);
      $items=$db->loadObjectList();
      if(count($items)<=0) die("It's now Clean");
      foreach($items as $obj) {
        $sSQL="DELETE FROM #__jf_content WHERE reference_table='content' AND reference_id = $obj->reference_id";
        $db->setQuery($sSQL);
        $db->query();
      }
    }
    
		// If i'm not viewing an article, then I search for articles that is missing translations, and do a translation of that article
		if(!is_object($this) || $_REQUEST[view]!="article") {
			$sSQL="SELECT * FROM #__content WHERE state>0 AND access=0 AND id NOT IN 
						(SELECT reference_id FROM #__jf_content WHERE reference_table='content' AND language_id=$dstlangid) LIMIT 1";
			$db->setQuery($sSQL);
			$jftarticle=$db->loadObject();
			if(!is_object($jftarticle)) return false; // no new articles pendant
		}
		if(is_numeric($_REQUEST[jftranslator])) {
			if($_REQUEST[jftranslatortoken]!=self::getToken()) {
				header("HTTP/1.0 401 Bad Token", true, 401);
				die();
			}
			//die("aca: ".$_REQUEST[jftranslatortoken]." ".self::getToken()." rq: ".$_REQUEST[jftranslator]);
			// For introtext translator, this send only the intro text
			if($_REQUEST[jftranslator]==1) {
				$jftarticle->introtext=$this->switchTags($jftarticle->introtext, $_REQUEST[tmpname]);
				die("<html><meta http-equiv=\"content-type\" content=\"text/html; charset=utf-8\" /><body>$jftarticle->introtext</body></html>");
			}
			// For fulltext translator, this send only the full text
			if($_REQUEST[jftranslator]==2) {
				$jftarticle->fulltext=$this->switchTags($jftarticle->fulltext, $_REQUEST[tmpname]);
				die("<html><meta http-equiv=\"content-type\" content=\"text/html; charset=utf-8\" /><body>$jftarticle->fulltext</body></html>");
			}
			// Test if it works with all fields separated by a special tag... do not use until it is ready
			if($_REQUEST[jftranslator]==3) {
				$campos=array("title","alias","metakey","metadesc","introtext","fulltext");
				echo "<html><meta http-equiv=\"content-type\" content=\"text/html; charset=utf-8\" /><body>";
				foreach($campos as $field) {
					$tmp=$this->switchTags($jftarticle->$field);
					$jftarticle->$field=$tmp[0];
					$tags[$field]=$tmp[1];
					$tagname="jfgtrl_".md5($field)."";
					echo "<$tagname>".$jftarticle->$field."</$tagname>";
				}
				if(($fp=fopen($_REQUEST[tmpname],"w"))==true) {
					fwrite($fp,serialize($tags));
					fclose($fp);
				}
				die("<jfgt_vrfy_tag /></body></html>");
			}
			die();
			//if(!strstr($_SERVER[HTTP_X_FORWARDED_FOR],"189.217.57.78")) return false;
		}
		

		// do a pause between translations to avoid ban. This should be a parameter
		if(time()-filemtime($this->basepath)<$this->delaytime || @filemtime($this->basepath."/banned.locked")>time()) return false;
		$campos=array(1=>"title","alias","metakey","metadesc");
	
		$sSQL="SELECT * FROM #__jf_content WHERE reference_table='content' AND reference_id=$jftarticle->id AND language_id=$dstlangid";
		$db->setQuery($sSQL);
		$jfarticle=$db->loadObjectList();
		// If count==0 then no translation is done, i do translation right now
		if(count($jfarticle)==0) {
			$campos=array(1=>"title","alias","metakey","metadesc","introtext","fulltext");
			$sometranslated=true;
			if(@strstr($_SERVER[HTTP_X_FORWARDED_FOR],$this->onlyfromthis) || @strstr($_SERVER[REMOTE_ADDR],$this->onlyfromthis)  || $this->onlyfromthis=="" || $this->onlyonbots==1) {
				$translation_text=$this->blockTranslate(3, $srclang, $dstlang, $jftarticle->id);
        if($translation_text==false) {
          if($this->debug) echo "JFGT Translation Error detected<br>";
          return false;
        }
				touch($this->basepath); // set basepath as modified
				if(strlen($translation_text)>0 && $translation_text!=false) { // Validate that something comes from translator
					foreach($campos as $field) {
						$tagname="jfgtrl_".md5($field)."";
						$ini=strpos($translation_text, "<$tagname>")+strlen("<$tagname>");
						$end=strpos($translation_text, "</$tagname>")-$ini;
						//echo "Parseando $tagname ($ini longitud $end)<br>";
						$narticle->$field=substr($translation_text, $ini, $end);					
					}
					$sometranslated=false;
				}
				//echo "<pre>";print_r($narticle); die("</pre>");
			}
		}
		//touch($this->basepath); // set basepath as modified
		if($_REQUEST[dbg]==1) {
			echo "<pre>";
			print_r($jfarticle);
			print_r($narticle);
			echo "</pre>";
			die();
		}
		
		// To show the progress and to add translations I add this fields
		//$campos[]="introtext";
		//$campos[]="fulltext";
		// This show the progress
		//$pasomax=0; foreach($campos as $campo) if(trim($jftarticle->$campo)!="") $pasomax++;
		//$paso=0; foreach($campos as $campo) if(trim($narticle->$campo)!="" && trim($jftarticle->$campo)!="") $paso++;
		//echo "<a href='".JURI::root()."index.php?option=com_content&task=view&view=article&id={$jftarticle->id}&lang={$dstlang}'>JFGT ART $jftarticle->id $srclang =&gt; $dstlang ($paso/$pasomax)</a><br>";
		if($this->debug) echo "<a href='".JURI::root()."index.php?option=com_content&task=view&view=article&id={$jftarticle->id}&lang={$dstlang}'>JFGT (".date("Ymd-H:i:s")." {$this->lastenc} to UTF-8) ART $jftarticle->id $srclang =&gt; $dstlang</a><br>";
		// if i got narticle here, then means that the article is successfully translated, so i update joomfish database
		//if(is_object($narticle) && $paso>=$pasomax && $sometranslated!=true) {
		if(is_object($narticle) && $sometranslated!=true) {
//			echo "<xmp>"; print_r($narticle); echo "</xmp>";
			//$campos[]="introtext";
			//$campos[]="fulltext";
			foreach($campos as $field) {
				/*
				unset($obj);
				$obj->language_id=$dstlangid;
				$obj->reference_id=$jftarticle->id;
				$obj->reference_table='content';
				$obj->reference_field=$field;
				$obj->value=$narticle->$field;
				$obj->original_text=$jftarticle->$field;
				$obj->original_value=md5($jftarticle->$field);
				$obj->modified="NOW()";
				$obj->modified_by=63;
				$obj->published=1;
				$ret=$db->insertObject("#__jf_content", $obj, null, true);
				if(!$ret) echo $db->stderr();
				*/
				// Remove scripts from NON fulltext/introtext fields
				if($field!="introtext" && $field!="fulltext") {
				  do {
				    $pos=strpos("<script",$narticle->$field);
				    if($pos!==true) break;
				    $end=strpos("</script>",$narticle->$field,$pos);
				    if($end!==true) break;
				    $narticle->$field=substr($narticle->$field, $pos, $end-$pos);
                                  } while($pos>=0);
				}
				$sSQL=sprintf("INSERT INTO #__jf_content(language_id, reference_id, reference_table, reference_field, value, 
						original_value, original_text, modified, modified_by, published) 
						VALUES($dstlangid, $jftarticle->id, 'content', '{$field}', %s, '%s', %s, NOW(), 63, %d)",
						$db->quote(self::utf8_encode($narticle->$field, $dstlang)),
						md5($jftarticle->$field),
						$db->quote($jftarticle->$field), 
						$this->autopublish
						);
				
				$db->setQuery($sSQL);
				$db->query();
				//echo "<xmp>$sSQL</xmp><br>";
			}
			//die("Traduccion posible");
		}
		return true;
	}
	
	function getTranslation($src, $lang) {
		if(isset($this->basepath)) {
			//echo "TRSRC: ".$this->basepath.DS.$lang.DS.md5($src)."<br>";
			if(($fp=@fopen($this->basepath.DS.$lang.DS.md5($src),"r"))!=false) {
				$ret=fread($fp,filesize($this->basepath.DS.$lang.DS.md5($src)));
				fclose($fp);
				return $ret;
			}
		}
		return false;
	}
	
	
	function updateTranslation($src, $dst, $lang) {
		if(strlen($dst)==0) {
			echo "Empty translation";
			return false;
		}
		$registry =& JFactory::getConfig();
		//$locale = $registry->getValue('config.language'); // user language
		$langparams = JComponentHelper::getParams('com_languages');
		$registry->setValue("config.defaultlang",$langparams->get("site"));
		$locale = $registry->getValue('config.defaultlang'); // default language
		$jfm =&  JoomFishManager::getInstance();
		$activeLanguages = $jfm->getActiveLanguages();		
		
		foreach($activeLanguages as $language=>$langvalues) {
			list($simplelang,)=split("\-",$language);
			if($language!=$locale && $langvalues->active==1 && $simplelang==$lang) {
				// Creates translation cache
				if(isset($this->basepath)) {
					//echo "TRDST: ".$this->basepath.DS.$language.DS.md5($src)."<br>";
					if(($fp=fopen($this->basepath.DS.$language.DS.md5($src),"w"))!=false) {
						fwrite($fp,$dst);
						fclose($fp);
						return true;
					}
				}
			}
		}
		return false;
	}
	
    function setOpts($opts) {
        if($opts["text"] != "") $this->opts["text"] = $opts["text"];
        if($opts["language_pair"] != "") $this->opts["language_pair"] = $opts["language_pair"];
    }

    function utf8_encode($str, $lang="en") {
      //if(eregi("(".LANGS_TO_UTF8.")", $lang)) return utf8_encode($str);
      return $str;
//      if(eregi("(he|ru)",$lang)) return $str;
//      return utf8_encode($str);
    }
    
    function findEncoding($src) {
      preg_match_all("/charset=([^\"]+)\"/i", $src, $enc);
      
      // Check for special charsets and get the right name of them
      $charsets=split(";",CHARSET_CONVERSIONS);
      foreach($charsets as $c) {
        list($csrc,$cdst)=split("\|",$c);
        if(strtoupper($enc[1][0])==$csrc) $enc[1][0]=$cdst;
      }      
      if(trim($enc[1][0])=="") {
        ob_start();
        echo "<xmp>";
        print_r($charsets);
        echo "****";
        print_r($enc);
        echo "</xmp>";
        $d=ob_get_clean();
        die("JF SRC-CHAR: empty: $d");
      }
      if(isset($this)) $this->lastenc=$enc[1][0]; // If possible, set in this->lastenc the encoding, for debugging
//      print_r($enc); die();
      if(function_exists("mb_convert_encoding")) { // Check if is installed mb_string
        $ret=mb_convert_encoding($src, "UTF-8", $enc[1][0]) or die("JF SRC-CHAR: ".$enc[1][0]);
        if(trim($ret)!="") return $ret; // If i got the right value, then return the encoded string
      }
      if(function_exists("iconv")) { // Check if is installed iconv library
        return iconv($enc[1][0], "UTF-8", $src);
      }
      die("Error! You need mb_string or iconv PHP Modules for JoomFish Google Translator plugin to work");
    }
	function blockTranslate($tipo=1, $srclang, $dstlang, $article_id=null) {
		// Build the translation URL
		$tmpname=tempnam($this->basepath.DS,"jfgoogletranslator_");
		if($article_id==null) {
			$url=JURI::root()."index.php".urlencode("?".$_SERVER[QUERY_STRING]."&jftranslator=$tipo&tmpname=".$tmpname."&jftranslatortoken=".self::getToken());
		} else {
			$url=JURI::root()."index.php".urlencode("?option=com_content&task=view&view=article&id=$article_id&jfartid=$article_id&jftranslator=$tipo&tmpname=".$tmpname."&jftranslatortoken=".self::getToken());
		}
		// Get the frame from google translator
		$geturl="http://translate.google.com/translate?hl=en&sl=$srclang&tl=$dstlang&u=".$url;
		@file_put_contents($this->basepath."/google-link.txt", $geturl."\n", FILE_APPEND);
		$frameset=$this->gltr_http_get_content("http://translate.google.com/translate?hl=en&sl=$srclang&tl=$dstlang&u=".$url);   
    // Gome some type of error in translation
    if(strstr($frameset,"{translationerror}")) return false;
		// Get both links, the upper and lower frames
		preg_match_all("/\<frame src=\"([^\"]+)\"[^\>]*\>/i",$frameset,$found);
		// Get only the second frame
		$found[1][1]=str_replace("&amp;","&",$found[1][1]);
		$fullhtml=$this->gltr_http_get_content("http://translate.google.com".$found[1][1]);
    if(strstr($frameset,"{translationerror}")) return false;
		$fullhtml=self::findEncoding($fullhtml);
		$cleanhtml=$this->gltr_clean_translated_page($fullhtml, $srclang);
		$cleanhtml=str_replace("&#39;","'",$cleanhtml);
		@file_put_contents($this->basepath."/traduccion-full.txt",$fullhtml);
		@file_put_contents($this->basepath."/traduccion-clean.txt",$cleanhtml);
		$tags=@file_get_contents($tmpname);
		if(strlen($tags)>0) {
			$tags=unserialize($tags);
			unlink($tmpname);
			if(is_array($tags)) {
				foreach($tags as $k=>$v) {
					if(is_array($v)) { // In case when the tags are from a full article translation
						foreach($v as $k1=>$v1) {
							$cleanhtml=str_replace("<$k1>", $v1, $cleanhtml);
						}
					} else {
						$cleanhtml=str_replace("<$k>", $v, $cleanhtml);
					}
				}
			}
		} else {
			unlink($tmpname);
		}
		@file_put_contents($this->basepath."/traduccion-tags.txt",$cleanhtml);
		if(strstr($cleanhtml,"We're sorry")) { // Detect banned site
			touch($this->basepath, time()+86400); // block updates for 24 hours
			touch($this->basepath."/banned.locked", time()+86400);
			return false;
		}
		if(eregi("Sorry, we are unable to access the page you requested.*Back to Translate.*See original page", $cleanhtml)) { // Detect error in request
			touch($this->basepath, time()+600); // block updates for 10 minutes
			touch($this->basepath."/banned.locked", time()+600);
			return false;
		}      
		if(eregi("(".LANGS_TO_HTMLDECODE.")", $dstlang)) {
		    $langcleanhtml=html_entity_decode($cleanhtml, ENT_COMPAT, "UTF-8");
		    $cleanhtml=html_entity_decode($cleanhtml, ENT_COMPAT, "UTF-8");
		    @file_put_contents($this->basepath."/traduccion-langclean.txt",$langcleanhtml);
		}
    if(!strstr($cleanhtml,"<jfgt_vrfy_tag />")) return false; // If this tag isn't on the translation, then the translation failed
    $cleanhtml=str_replace("<jfgt_vrfy_tag />","",$cleanhtml); // Removes the translation verify tag
		//echo "<xmp>$fullhtml</xmp>otro<xmp>$cleanhtml</xmp>fin"; die();		
//		die("Antes: ".$cleanhtml."<br><br>Despues: ".$langcleanhtml);
		return $cleanhtml;
	}

    function translate() {
        $this->out = "";
        $google_translator_url = "http://translate.google.com/translate_t?langpair=".urlencode($this->opts["language_pair"])."&amp;";
		//echo "traductor: $google_translator_url,....".$this->opts["language_pair"];
        $google_translator_data .= "text=".urlencode($this->opts["text"]);
        $gphtml = $this->postPage(array("url" => $google_translator_url, "data" => $google_translator_data));
        $out = substr($gphtml, strpos($gphtml, "<div id=result_box dir=\"ltr\">"));
        $out = substr($out, 29);
        $out = substr($out, 0, strpos($out, "</div>"));
        //$this->out = utf8_encode($out);
        $out=str_replace("&#39;","'",$out);
        $this->out=$out;
        return $this->out;
    }

    // post form data to a given url using curl libs
    function postPage($opts) {
        $html = "";
        if($opts["url"] != "" && $opts["data"] != "") {
            $ch = curl_init($opts["url"]);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_HEADER, 1);
            //curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
            curl_setopt($ch, CURLOPT_TIMEOUT, 15);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_COOKIESESSION, 1);
            curl_setopt($ch, CURLOPT_REFERER, "http://translate.google.com/translate_t#");
            curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Linux; U; Intel Linux; en-US; rv:1.8.1) Gecko/20061010 Joomla Google Translator/1.0");
            curl_setopt($ch, CURLOPT_POSTFIELDS, $opts["data"]);
            $html = curl_exec($ch);
            if(curl_errno($ch)) {
              die(sprintf("Error traduciendo: (%d) [%s]",curl_errno($ch), curl_error($ch)));
              $html = "";
            }
            if(strstr($html,"sorry.google.com")) {
              echo("Error: google ya no te va a dejar traducir hasta que pase un rato<br>");
              list($head,$code)=split("<",$html,2);
              die("<pre>$head</pre><$code");
            }
            curl_close ($ch);
        }
        return $html;
    }
	
	/**
	 * This functions splits the src text into pharagraphs, with length less than 500 characters, without
	 * any HTML tag. This splitted text should serve for google translator services
	 * 
	 * steps for splitText (used for google translator)
	 * 1- removes all the HTML tags
	 * 2- runs the text until:
	 * 	2.1- a dot OR EOL OR EOF is found. If dot, then with an space or enter after it to indicate phrase separator
	 *  2.1.1- if a quote is found then i stop also there
	 *  2.2- 500 characters limit 
	 * 
	 * steps for splitText+marker (used to translate text preserving tags)
	 * 1- replaces all the HTML tags with a marker that google doesn't translate, and link that marker with that tag
	 * 2- returns, two arrays, one containing the phrases and the other the markers
	 */
	function splitText($src, $removetags=0, $paskey=false) {
		$src=utf8_decode($src);
		if($removetags==1) {
			$src=str_replace("\r\n"," ",$src);
			$src=str_replace("\n\r"," ",$src);
			$src=str_replace("\r"," ",$src);
			$src=str_replace("\n"," ",$src);
			$src=preg_replace('/\<li*\>/i', " ", $src);
			$src=preg_replace('/\<td*\>/i', " ", $src);
			$src=preg_replace('/\<th*\>/i', " ", $src);
			$src=strip_tags($src,"br");
			$src=preg_replace('/\<br(\s*)?\/?\>/i', "\n", $src);
		}
		
		if(strlen($src)<500) {
			return array(utf8_encode($src));
		}
		
		if($removetags==1 || 1) {
			$pos=0;
			while($pos<strlen($src)) {
				for($p=0;$p<500;$p++) {
					if($src[$pos+$p]=="." && ($src[$pos+$p+1]=="\r" || $src[$pos+$p+1]==" " || $pos+$p>=strlen($src)) ||
					   $src[$pos+$p]=="\n" || $pos+$p>=strlen($src) ||
					   $src[$pos+$p]=="\"") {
						$f=trim(substr($src, $pos, $p));
						break;
					}
				}
				$pos+=$p+1;
				if(trim($f)!="") {
					//echo "$pos, $p, $f<br>";
					$f=utf8_encode($f);
					if($paskey==true)
						$frases[$f]="";
					else
						$frases[]=$f;
				}
			}
			return $frases;
		}
		
		unset($portags);
		unset($par);
		
		//if($removelinks==1) $src=eregi_replace("<(\/a|a[^>]+)>","",$src);
		//$portags=split("<[^(\/a|a)]{1}[^>]+>",$src);
		$portags=split("<[^>]+>",$src);
		foreach($portags as $txt) {
			if(strchr($txt,".")) {
				$txt=trim($txt);
				$porpuntos=split("\.",$txt);
				if(count($porpuntos)>1) {
					foreach($porpuntos as $k=>$frase) {
						if(trim($frase)!="") 
							if($k<count($porpuntos)) $par[]=trim($frase).""; else $par[]=trim($frase);
					}
				} else {
					$par[]=trim($txt);
				}
			} elseif(trim($txt)!="") {
				$par[]=trim($txt);
			} 
		}   
		
		//print_r($par);
		foreach($par as $p) {
			$p=eregi_replace("([a-z]) '([a-z][^a-z])","\\1'\\2",$p);
			$p=utf8_encode($p);
			if(!isset($frases[$p])) {
				$frases[$p]="";
			}
		}  
		if($paskey==1) return $frases;
		foreach($frases as $frase=>$v) 
			$ret[]=$frase;
		return $ret;
	}
	
	function translateText($src) {
		$frases=$this->splitText($src, true, true);
		$dstlang="en";
		foreach($frases as $k=>$v) $frases[$k]=$this->getTranslation($v, $dstlang);
		$found=$notfound=$foundcased=0;
		$translation=$src;
		foreach($frases as $k=>$p) {
			$k=trim($k);
			$id=md5($k);
			if(strlen($k)>2) {
				if(strstr($translation,$k)) {
					$translation=str_replace($k,"$p",$translation);
					$found++;
				} elseif(stristr($translation,$k)) {
					$translation=str_replace($k,$p,$translation);
					$foundcased++;
				} else {
					$notfound++;
				}
			}  
		}
		return $translation;
	}
	
	function gltr_patch_translation_url($res) {
		
	  if (TRANSLATION_ENGINE == 'google'){
		$maincont = gltr_http_get_content($res);
		$matches = array();
		preg_match( '/(\/translate_p[^"]*)"/',$maincont,$matches);
		$res = "http://translate.google.com" . $matches[1];
		$res = str_replace('&amp;','&', $res);    
		gltr_debug("gltr_patch_translation_url :: Google Patched: $res");
		
	  } else if (TRANSLATION_ENGINE == 'babelfish'){
		$maincont = gltr_http_get_content( $res);
		$matches = array();
		preg_match( '/URL=(http:\/\/[0-9\.]*\/babelfish\/translate_url_content[^"]*)"/',$maincont,$matches);
		$res = $matches[1];
		$res = str_replace('&amp;','&', $res);    
		gltr_debug("gltr_patch_translation_url :: Babelfish Patched: $res");
		
	  } else if (TRANSLATION_ENGINE == 'freetransl'){
		$tmp_buf = gltr_http_get_content("http://www.freetranslation.com/");
		$matches = array();
		preg_match('/<input type="hidden" name="username" id = "hiddenUsername" value="([^"]*)" \/>[^<]*<input type="hidden" name="password" id = "hiddenPassword" value="([^"]*)" \/>/',$tmp_buf,$matches);      
		$res .= "&username=$matches[1]&password=$matches[2]";
		gltr_debug("gltr_patch_translation_url :: FreeTransl Patched: $res");
	  }
		return $res;
	}
	function gltr_build_translation_url($srcLang, $destLang, $urlToTransl) {
	  global $gltr_engine;
	  if (TRANSLATION_ENGINE == 'google'){
		$urlToTransl = urlencode($urlToTransl);  
	  }else if (TRANSLATION_ENGINE == 'babelfish'){	
			$urlToTransl = urlencode($urlToTransl);   
		}
	  $tokens = array('${URL}', '${SRCLANG}', '${DESTLANG}');
	  $srcLang = $gltr_engine->decode_lang_code($srcLang);
	  $destLang = $gltr_engine->decode_lang_code($destLang);
	  $values = array($urlToTransl, $srcLang, $destLang);
	  $res = str_replace($tokens, $values, $gltr_engine->get_base_url());
	  return $res;
	} 
	function gltr_clean_url_to_translate(){
	  $url = gltr_get_self_url();
	  $url_to_translate = "";

	  $blog_home_esc = BLOG_HOME_ESCAPED;

	  if (REWRITEON) {
		$contains_index = (strpos($url, 'index.php')!==false);
		if ($contains_index){
		  $blog_home_esc .= '\\/index.php';
		}
		$pattern1 = '/(' . $blog_home_esc . ')(\\/(' . LANGS_PATTERN . ')\\/)(.+)/';
		$pattern2 = '/(' . $blog_home_esc . ')\\/(' . LANGS_PATTERN . ')[\\/]{0,1}$/';

		if (preg_match($pattern1, $url)) {
		  $url_to_translate = preg_replace($pattern1, '\\1/\\4', $url);
		} elseif (preg_match($pattern2, $url)) {
		  $url_to_translate = preg_replace($pattern2, '\\1', $url);
		}
		gltr_debug("gltr_clean_url_to_translate :: [REWRITEON] self url:$url | url_to_translate:$url_to_translate");

	  } else {
		$url_to_translate = preg_replace('/[\\?&]{0,1}lang\\=(' . LANGS_PATTERN . ')/i', '', $url);
		gltr_debug("gltr_clean_url_to_translate :: [REWRITEOFF] self url:$url | url_to_translate:$url_to_translate");
	  }
	  return $url_to_translate;
	}

	function gltr_make_server_redirect_page($resource){
		if (isset($_GET['gltr_redir'])){		
			$unavail =
				'<html><head><title>Translation not available</title>
				<META NAME="ROBOTS" CONTENT="NOINDEX, NOFOLLOW">
				<style>html,body {font-family: arial, verdana, sans-serif; font-size: 14px;margin-top:0px; margin-bottom:0px; height:100%;}</style></head>
				<body><center><br /><br /><b>This page has not been translated yet.<br /><br />The translation process could take a while: in the meantime a semi-automatic translation will be provided in a few seconds.</b><br /><br /><a href="'.get_settings('home').'">Home page</a></center>
				<script type="text/javascript"><!--
				setTimeout("Redirect()",5000);
				function Redirect(){
				 location.href = "{RESOURCE}";
				}
				// --></script></body></html>';
		  $message = str_replace('{RESOURCE}',$resource,$unavail);   
		} else {
			$redirect = gltr_add_get_param(gltr_get_self_url(), 'gltr_redir', urlencode($resource));
			gltr_debug("gltr_make_server_redirect_page :: redirecting to $redirect");
			header("Location: $redirect", TRUE, 302);	
			die();
	  }
	  return $message;
	}

	function gltr_add_get_param($url,$param, $value){
	  if (strpos($url,'?')===false)
		$url .= "?$param=$value";
	  else
		$url .= "&$param=$value";
	  return $url;
	}

	function gltr_translate($lang) {
	  global $gltr_engine;
	  
	  $page = "";
	  $url_to_translate = gltr_clean_url_to_translate();
	  $resource = gltr_build_translation_url(BASE_LANG, $lang, $url_to_translate);

	  if (!gltr_is_connection_allowed()){
		$page = gltr_make_server_redirect_page($resource);
	  } else {
		  $buf = gltr_http_get_content(gltr_patch_translation_url($resource));
			if (gltr_is_valid_translated_content($buf)){
			gltr_store_translation_engine_status('working');
				$page = gltr_clean_translated_page($buf, $lang);
			} else {
			gltr_store_translation_engine_status('banned');
			gltr_debug("Bad translated content for url: $url_to_translate \n$buf");
			$page = gltr_make_server_redirect_page($resource);
			}
	  }
	  return $page;
	}

	function gltr_http_get_content($resource) {
	  $isredirect = true;
	  $redirect = null;
		
		while ($isredirect) {
		$isredirect = false;
		if (isset($redirect_url)) {
		  $resource = $redirect_url;
		}

		$url_parsed = parse_url($resource);
		$host = $url_parsed["host"];
		$port = $url_parsed["port"];
		if ($port == 0)
		  $port = 80;
		$path = $url_parsed["path"];
		if (empty($path))
		  $path = "/";
		$query = $url_parsed["query"];
		$http_q = $path . '?' . $query;

		$req = $this->gltr_build_request($host, $http_q);
					
		$fp = @fsockopen($host, $port, $errno, $errstr);

		if (!$fp) {
		  return "{translationerror} $errstr ($errno)<br />\n";
		} else {
		  fputs($fp, $req, strlen($req)); // send request
		  $buf = '';
		  $isFlagBar = false;
		  $flagBarWritten = false;
		  $beginFound = false;
		  $endFound = false;
		  $inHeaders = true;
				$prevline='';
		  while (!feof($fp)) {
			$line = fgets($fp);
			if ($inHeaders) {
				
			  if (trim($line) == '') {
				$inHeaders = false;
				continue;
			  }

			  $prevline = $line;
			  if (!preg_match('/([^:]+):\\s*(.*)/', $line, $m)) {
				// Skip to the next header
				continue;
			  } 
			  $key = strtolower(trim($m[1]));
			  $val = trim($m[2]);
						if ($key == 'location') {
				$redirect_url = $val;
				$isredirect = true;
				break;
			  }
			  continue;
			}
					
			$buf .= $line;
		  } //end while
		}
		fclose($fp);
	  } //while($isredirect) 
	  return $buf; 
	}


	function gltr_is_valid_translated_content($content){
		return (strpos($content, FLAG_BAR_BEGIN) > 0);
	}

	function gltr_store_translation_engine_status($status){
		$exists = get_option("gltr_translation_status");
		if($exists === false){ 
			add_option("gltr_translation_status","unknown");
		}
		update_option("gltr_translation_status",$status);	
	}

	function gltr_is_connection_allowed(){

		$last_connection_time = get_option("gltr_last_connection_time");
		if($last_connection_time === false){ 
			add_option("gltr_last_connection_time",0);
			$last_connection_time = 0;
		} 
		
		if ($last_connection_time > 0){
			$now = time();
			$delta = $now - $last_connection_time;
			if ($delta < CONN_INTERVAL){
				gltr_debug("gltr_is_connection_allowed :: Blocking connection request: delta=$delta secs");
				$res = false;
			} else {
				gltr_debug("gltr_is_connection_allowed :: Allowing connection request: delta=$delta secs");
				update_option("gltr_last_connection_time", $now);
			$res = true;
		  }
		} else {
			gltr_debug("gltr_is_connection_allowed :: Warning: 'last_connection_time' is undefined: allowing translation");
			update_option("gltr_last_connection_time", time());
			$res = true;
		}
		return $res;
	}

	function gltr_clean_link($matches){
		if (TRANSLATION_ENGINE == 'google'){
			$res = "=\"" . urldecode($matches[1]) . $matches[3] . "\"";
			if ($matches[4] == '>') $res .= ">";
		} else {
			$res = "=\"" . urldecode($matches[1]) . "\"";
		}
		//echo ":::: <xmp>$res</xmp> ::::";
		return $res;
	}

	function gltr_clean_translated_page($buf, $lang) {
		global $gltr_engine;
		global $well_known_extensions;  

		$is_IIS = (strpos($_SERVER['SERVER_SOFTWARE'], 'Microsoft-IIS') !== false) ? true : false;

		if (TRANSLATION_ENGINE == 'google' && 0) {
			$buf = preg_replace("/<iframe src=\"http:\/\/translate\.google\.com\/translate_un[^>]*><\/iframe>/i", "",$buf);
		}

		$patterns=   array(
							"/=[^\s|>|\"]*u=([^\?|&]+)&amp;([^\s|>|#|\"]*)([#]{0,1}[^\s|>|\"]*)([\s|>|\"]{1})/",
							//"/=[^\s|>|\"]*u=(.*[\?]{0,1})&amp;([^\s|>|#|\"]*)([#]{0,1}[^\s|>|\"]*)([\s|>|\"]{1})/",
  						  );

		foreach( $patterns as $id => $pattern){
			//echo $pattern;
			//preg_match_all($pattern, $buf, &$encontrado); echo "<xmp>";print_r($encontrado);echo "</xmp>"; //echo urldecode($encontrado[1]).$encontrado[3];
			$buf = preg_replace_callback($pattern, array('self','gltr_clean_link'), $buf);
		}
		//die("ntro aca <xmp>$buf</xmp>");
		$buf = preg_replace("/<meta name=\"description\"([ ]*)content=\"([^>]*)\"([ ]*)\/>/i", "", $buf);
		$buf = preg_replace("/<meta name='description'([ ]*)content='([^>]*)'([ ]*)\/>/i", "", $buf);
		//TODO: add <meta name="language" content="LANG" />


		$blog_home_esc = BLOG_HOME_ESCAPED;
		$blog_home = BLOG_HOME;

		if (REWRITEON) {
			if ($is_IIS){
				$blog_home_esc .= '\\/index.php';
				$blog_home .= '/index.php';
				$pattern = "/<a([^>]*)href=\"" . $blog_home_esc . "(((?![\"])(?!\/trackback)(?!\/feed)" . $this->gltr_get_extensions_skip_pattern() . ".)*)\"([^>]*)>/i";
				$repl = "<a\\1href=\"" . $blog_home . '/' . $lang . "\\2\" \\4>";
				//gltr_debug("IS-IIS".$repl."|".$pattern);
				$buf = preg_replace($pattern, $repl, $buf);
			} else {
				$pattern = "/<a([^>]*)href=\"" . $blog_home_esc . "(((?![\"])(?!\/trackback)(?!\/feed)" . $this->gltr_get_extensions_skip_pattern() . ".)*)\"([^>]*)>/i";
				$repl = "<a\\1href=\"" . $blog_home . '/' . $lang . "\\2\" \\4>";
				//gltr_debug($repl."|".$pattern);
				$buf = preg_replace($pattern, $repl, $buf);
			}
		} else {
			$pattern = "/<a([^>]*)href=\"" . $blog_home_esc . "\/\?(((?![\"])(?!\/trackback)(?!\/feed)" . $this->gltr_get_extensions_skip_pattern() . ".)*)\"([^>]*)>/i";
			$repl = "<a\\1href=\"" . $blog_home . "?\\2&lang=$lang\" \\4>";
			$buf = preg_replace($pattern, $repl, $buf);

			$pattern = "/<a([^>]*)href=\"" . $blog_home_esc . "[\/]{0,1}\"([^>]*)>/i";
			$repl = "<a\\1href=\"" . $blog_home . "?lang=$lang\" \\2>";
			$buf = preg_replace($pattern, $repl, $buf);
		}

		//let's remove custom tags added by certain engines
		if (TRANSLATION_ENGINE == 'promt') {
			//$buf = preg_replace("/\<div class='PROMT_HEADER'(.*)\<\/div\>/i", "", $buf);
			//$buf = preg_replace("/\<span class=\"UNKNOWN_WORD\"\>([^\<]*)\<\/span\>/i", "\\1",$buf);
			$buf = preg_replace("/onmouseout=\"OnMouseLeaveSpan\(this\)\"/i", "",$buf);
			$buf = preg_replace("/onmouseover=\"OnMouseOverSpanTran\(this,event\)\"/i", "",$buf);
			$buf = preg_replace("/<span class=\"src_para\">/i", "<span style=\"display:none;\">",$buf);
		} else if (TRANSLATION_ENGINE == 'freetransl') {
			$buf = preg_replace("/\<div(.*)http:\/\/www\.freetranslation\.com\/images\/logo\.gif(.*)\<\/div\>/i", "", $buf);
			$buf = str_replace(array("{L","L}"), array("",""), $buf);
		} else if (TRANSLATION_ENGINE == 'google') {
			$buf = preg_replace("/<iframe src=\"http:\/\/translate\.google\.com\/translate_un[^>]*><\/iframe>/i", "",$buf);
			$buf = preg_replace("/<script>[^<]*<\/script>[^<]*<script src=\"[^\"]*translate_c.js\"><\/script>[^<]*<script>[^<]*_intlStrings[^<]*<\/script>[^<]*<style type=[\"]{0,1}text\/css[\"]{0,1}>\.google-src-text[^<]*<\/style>/i", "",$buf);
			$buf = preg_replace("/_setupIW\(\);_csi\([^\)]*\);/","",$buf);
			$buf = preg_replace("/onmouseout=[\"]{0,1}_tipoff\(\)[\"]{0,1}/i", "",$buf);
			$buf = preg_replace("/onmouseover=[\"]{0,1}_tipon\(this\)[\"]{0,1}/i", "",$buf);
			$buf = preg_replace("/<span class=[\"]{0,1}google-src-text[\"]{0,1}[^>]*>/i", "<span style=\"display:none;\">",$buf);
			$buf = preg_replace("/<span style=\"[^\"]*\" class=[\"]{0,1}google-src-text[\"]{0,1}[^>]*>/i", "<span style=\"display:none;\">",$buf);


			$buf = preg_replace("/<iframe [^>]*><\/iframe>/i", "",$buf);
			$buf = preg_replace("/<a set=[^>]*>[\s]*<\/a>/i", "",$buf);
			$buf = preg_replace("/<body[^>]*>/i", "",$buf);
			$buf = preg_replace("/<html[^>]*>/i", "",$buf);
			$buf = preg_replace("/<meta[^>]*>/i", "",$buf);
			$buf = preg_replace("/<head[^>]*>/i", "",$buf);
			$buf = preg_replace("/<base[^>]*>/i", "",$buf);
			$buf = preg_replace("/<\/head[^>]*>/i", "",$buf);
			$buf = preg_replace("/<\/body[^>]*>/i", "",$buf);
			$buf = preg_replace("/<\/html[^>]*>/i", "",$buf);
			$buf = preg_replace("/\?jftranslator=[0-9]/i", "",$buf);
			$buf = preg_replace("/<script>[^<]*<\/script>/i", "",$buf);
			$buf = preg_replace("/src=images/i", "src=/images",$buf);
		}
	  
		if (HARD_CLEAN){
			$out = array();
			$currPos=0;
			$result = "";
			$tagOpenPos = 0;
			$tagClosePos = 0;
			
			while (!($tagOpenPos === false)){
				$beginIdx = $tagClosePos;
				$tagOpenPos = stripos($buf,"<span style=\"display:none;\">",$currPos);
				$tagClosePos = stripos($buf,"</span>",$tagOpenPos);
				if ($tagOpenPos == 0 && ($tagOpenPos === false) && strlen($result) == 0){
					//gltr_debug("===>break all!");
					$result = $buf;
					break;
				}
				$offset = substr($buf,$tagOpenPos,$tagClosePos - $tagOpenPos + 7);
				preg_match_all('/<span[^>]*>/U',$offset,$out2,PREG_PATTERN_ORDER);
				$nestedCount = count($out2[0]);
				
				for($i = 1; $i < $nestedCount; $i++){
					$tagClosePos = stripos($buf,"</span>",$tagClosePos + 7);
				}
				if ($beginIdx > 0)$beginIdx += 7;
				
				$result .= substr($buf,$beginIdx,$tagOpenPos - $beginIdx);
				$currPos = $tagClosePos;
			}
			//gltr_debug($result);
			$buf = $result . substr($buf,$beginIdx);//Fixed by adding the last part of the translation: thanks Nick Georgakis!
		}
		if (TRANSLATION_ENGINE == 'google') {
			//echo "Begin ciclo<br>";
			//echo "Aqui: <xmp>$buf</xmp><br>";
			while(($tagOpenPos = stripos($buf,"<span  >"))!==false) {
				$tagClosePos = stripos($buf,"</span>",$tagOpenPos);
				if($tagClosePos===false) break;
				$buf=substr($buf,0,$tagOpenPos).substr($buf,$tagOpenPos+8,$tagClosePos-($tagOpenPos+8)).substr($buf,$tagClosePos+7);
				//echo "Aca: <xmp>$buf</xmp><br>";
			}
		}
		return $buf;
	}
	function gltr_get_extensions_skip_pattern() {
		$well_known_extensions=$this->well_known_extensions;
		
		$res = "";
		foreach ($well_known_extensions as $key => $value) {
			$res .= "(?!\.$value)";
		}
		return $res;
	}
	function gltr_get_random_UA(){
		global $gltr_ua;
		$tot = count($gltr_ua);
		$id = rand( 0, count($gltr_ua)-1 );
		$ua = $gltr_ua[$id];
		//gltr_debug("Random UA nr $id: $ua");
		return $ua;
	}

	function gltr_build_request($host, $http_req) {
	  $res = "GET $http_req HTTP/1.0\r\n";
	  $res .= "Host: $host\r\n";
	  $res .= "User-Agent: " . $this->gltr_get_random_UA() . " \r\n";
	  $res .= "Accept: text/xml,application/xml,application/xhtml+xml,text/html;q=0.9,text/plain;q=0.8,image/png,*/*;q=0.5\r\n";
	  $res .= "Accept-Charset: ISO-8859-1,utf-8;q=0.7,*;q=0.7\r\n";
	  $res .= "Connection: close\r\n";
	  $res .= "\r\n";
	  return $res;
	}
	
}

?>
