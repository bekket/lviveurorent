<?php

defined( '_JEXEC' ) or die( 'Restricted access' );

$folder 			= 	$params->get( 'folder', 'images/stories/fruit' );

$mootools 			= 	$params->get('mootools',1);

$swidth 			= 	$params->get( 'swidth', 430 );

$sheight 			= 	$params->get( 'sheight', 200 );

$altimage 			= 	$params->get( 'altimage', 'JA Slideshow - http://www.joomlart.com' );

$orderby 			= 	$params->get( 'sortCriteria', 0);

$sort 				= 	$params->get( 'sort', '1');

$setimagearray 		= 	$params->get( 'setimagearray', '');

$startwith 			= 	$params->get( 'startwith', 0);

$autoresize 		= 	$params->get( 'autoresize', 0);

$timedisplay 		= 	$params->get( 'timedisplay', 9000 );

$timeanimation 		= 	$params->get( 'timeanimation', 2000);

$animation 			= 	$params->get( 'animation', 'combo');

$ppercent 			= 	$params->get( 'ppercent', 10);

$zpercent 			= 	$params->get( 'zpercent', 10);

$effect 			= 	$params->get( 'effect', 'bounceOut');

$showCaption 		= 	$params->get( 'showCaption', 1 );

$showmode 			= 	$params->get( 'showmode', 0 );

$navigation 		= 	$params->get('navigation',"");

$showDescription 	= 	$params->get('showDescription',0);

$descriptions 		= 	$params->get('description',"");

$play		 		= 	$params->get('play',"play");


if($showDescription){
global $iso_client_lang;
$descriptionArr = preg_split('/<lang=([^>]*)>/', $descriptions , -1 , PREG_SPLIT_NO_EMPTY | PREG_SPLIT_DELIM_CAPTURE);
$description = '';
if(count($descriptionArr) > 1){
	for($i=0;$i<count($descriptionArr);$i=$i+2){
		if($descriptionArr[$i] == $iso_client_lang){
			$description = $descriptionArr[($i+1)];
			break;
		}
	}
	if(!$description){
		$description = $descriptionArr[1];
	}
}
else{
	$description = $descriptionArr[0];
}
$description = str_replace("<br />", "\n", $description);
 $description = explode("\n",$description);

 $descriptionArray = array();

	foreach($description as $desc){

		//~ $list = explode(":", $desc , 2);

		//~ $descriptionArray[$list[0]] = $list[1];

		if ($desc) {

			$list = split(":", $desc, 2);

			$list[1] = (count($list) > 1) ? split("&", $list[1]) : array();

			$temp = array();

			for ($i = 0; $i < count($list[1]); ++$i) {

				$l = split("=", $list[1][$i]);

				$temp[trim($l[0])] = trim($l[1]);

			}

			$descriptionArray[$list[0]] = $temp;

		}

	}

}




$folder = checkURL($folder);



if(!$folder){ echo "This folder doesn't exsits."; }

else{

	if (trim($setimagearray) != "")	$images = explode(",", $setimagearray);

	else $images = getFileInDir($folder, $orderby, $sort );



	if (count($images) > 0) {

		$imgcount = 0;

		$firstImage = '';

		$transDetails = '';

		$captionDetails = '';

		$imageArray = array();

		$captionArray = array();

		$listDescription = "";



		foreach($images as $img) {

			if ($imgcount++ == 0) {

				if ($startwith)

					$firstImage = $folder . $startwith;

				else

					$firstImage = $folder . $img;

			}

			$imageArray[] = "'$img'";

			if($showDescription) {

				$captionsArray[] = (isset($descriptionArray[$img]) && isset($descriptionArray[$img]['caption'])) ? str_replace("'", "\'", $descriptionArray[$img]['caption']) :'';

				$urlsArray[] = (isset($descriptionArray[$img]) && isset($descriptionArray[$img]['url'])) ? $descriptionArray[$img]['url'] :'';

			}

		}



		if ($showDescription) {

		 	$listCaption = " captions: ['" . implode("','", $captionsArray) . "'],";

		 	$listURL = " urls: ['" . implode("','", $urlsArray) . "'],";

		}




            if ($mootools) { echo '<script src="'.JURI::base().'/modules/mod_jaslideshow/ja_slideshow/mootools.v1.1.js" type="text/javascript"></script>' . "\n"; }


			if (trim($animation) == 'push' or trim($animation == 'wipe')) $transDetails = "transition: Fx.Transitions.$effect";

			else $transDetails = "transition: Fx.Transitions.sineInOut";



			echo '<script src="'.JURI::base().'/modules/mod_jaslideshow/ja_slideshow/ja-slideshow.js" type="text/javascript"></script>' . "\n";



			$linktag = '<link href="'.JURI::base().'/modules/mod_jaslideshow/ja_slideshow/ja-slideshow.css" rel="stylesheet" type="text/css" />';
            JHTML::stylesheet('ja-slideshow.css', JURI::base().'/modules/mod_jaslideshow/ja_slideshow/');

			$buffer = ob_get_contents();

			$pos = strpos ($buffer, '</head>');

			if (!strpos ($buffer, "ja-slideshow.css") && $pos) {

				$buffer = substr ($buffer, 0, $pos) . "\n$linktag\n". substr($buffer, $pos);

				ob_clean();

				echo $buffer;

			}



			echo '<div id="ja-slideshow-wrap">' . "\n";

				echo '<div id="ja-slideshow-case" class="ja-slideshow-case">' . "\n";

					echo '<img src="' . $firstImage . '" alt="' . $altimage . '" title="' .$altimage. '" />' . "\n";

				echo '</div>' . "\n";
//				echo '<div class="ja-slideshow-mask1">&nbsp;</div>';
//				echo '<div class="ja-slideshow-mask2">&nbsp;</div>';
//				echo '<div class="ja-slideshow-mask3">&nbsp;</div>';
//				echo '<div class="ja-slideshow-mask4">&nbsp;</div>';
				echo '<div id="ja-slidebar" class="ja-slidebar">' . "\n";
				echo '</div>' . "\n";
			echo '</div>' . "\n";



		?>

		<script type="text/javascript">

			JaSlideshow = new JaSlideshowClass({

					siteurl: '<?php echo JURI::base(); ?>',

					type: '<?php echo $animation; ?>',

					pan: '<?php echo $ppercent; ?>',

					zoom: '<?php echo $zpercent; ?>',

					width: <?php echo $swidth; ?>,

					height: <?php echo $sheight; ?>,

					url: '<?php echo $folder; ?>',

					images: [<?php echo implode(",", $imageArray); ?>],

					<?php if (isset($listCaption)) echo $listCaption; ?>

					<?php if (isset($listURL)) echo $listURL; ?>

					duration: [<?php echo $timeanimation*1000; ?>, <?php echo $timedisplay*1000; ?>],

					<?php echo $transDetails; ?> ,

					navigation: '<?php echo $navigation; ?>',

					resize: <?php echo $autoresize; ?>,

					classes: ['prev', 'next', 'active'],

					play : <?php echo "'$play'"; ?>,

					thumbnailre: [/\./, 't.']

					});

		</script>

		<?php

	}

}

function getFileInDir($folder, $orderby, $sort){


	$imagePath 	= JPATH_SITE ."/".$folder;

	$imgFiles 	= JFolder::files( $imagePath );

	$folderPath = $folder .'/';

	$imageFile = array();

	$i = 0;

	foreach ($imgFiles as $file){

		$i_f 	= $imagePath .'/'. $file;

		if ( eregi( "bmp|gif|jpg|png|jpeg", $file ) && is_file( $i_f ) ) {

			$imageFile[$i][0] = $file;

			$imageFile[$i][1] = filemtime($i_f)	;

			$i++;

		}

	}

	$images = sortImage($imageFile, $orderby , $sort);

	return $images;

}



function sortImage($image, $orderby , $sort){

	$sortObj = array();

	$imageName = array();

	if($orderby == 1){

		for($i=0;$i<count($image);$i++){

			$sortObj[$i] = $image[$i][1];

			$imageName[$i] = $image[$i][0];

		}

	}

	else{

		for($i=0;$i<count($image);$i++){

			$sortObj[$i] = $image[$i][0];

		}

		$imageName = $sortObj;

	}

	if($sort == 1) array_multisort($sortObj, SORT_ASC, $imageName);

	elseif($sort == 2)	array_multisort($sortObj, SORT_DESC, $imageName);

	else shuffle($imageName);

	return $imageName;

}



function checkURL($url){

	if(is_dir($url)){ $url = (substr($url,-1,1) == "/") ? $url : $url."/";	return $url; }

	else { return false; }

}



?>

