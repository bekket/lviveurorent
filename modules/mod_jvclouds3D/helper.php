﻿<?php
/*
*/

// no direct access
defined('_JEXEC') or die('Restricted access');

class mosJVClouds
{
//--------------------------Настройки внешнего вида облака
	//Количестов тегов
	var $_JC_count_tags=null;
	//Формат вывода тегов - списком или облаком
	var $_JC_format=null;
	//Зависимость размера шрифта тега от его рейтинга
	var $_JC_zav=null;
	//Сортировка
	var $_JC_sort=null;
	//Выравнивание облака
	var $_JC_text_align=null;
	//Вывод рейтинга тегов
	var $_JC_rating=null;
	//Максимальная длина тега в облаке
	var $_JC_max_length=null;
	//Минимальная длина тега в облаке
	var $_JC_min_length=null;
	//Максимальный размер шрифта тега
	var $_JC_max_font=null;
	//Минимальный размер шрифта тега
	var $_JC_min_font=null;
	//Высота строки
	var $_JC_line_height=null;
	//Количество выборок при запросе в базу данных
	var $_JC_limit=null;
	//Процент похожести слов при использовании функции similar_text
	var $_JC_proc=null;
	//Процент содержания словосочетаний
	var $_JC_proc_slovosoch=null;
//--------------------------Настройки  разделов и категрий
	//Учитываемые категории
	var $_JC_cat=null;
	//Учитываемые разделы
	var $_JC_sec=null;
//--------------------------Настройки использования ключевых слов
	//Использовать ключевые слова
	var $_JC_meta_tags=null;
	var $_JC_meta_tags_rating=null;
//--------------------------Настройки дополнительных слов
	//Черный список (слова и словосочетания перечислены через запятую)
	var $_JC_black_words=null;
	//Белый список (слова и словосочетания перечислены через запятую)
	var $_JC_white_words=null;
//--------------------------Свойства класса не относящиеся к настройкам
	//Слова не попадающие в облако (союзы, предлоги т.д.)
	var $_JC_not_words = 'будто вроде да даже едва если же и или итак как как-то когда ли либо нежели но пока поскольку пускай пусть раз разве словно так также тоже точно хоть хотя чем что чтоб чтобы кроме через то со среди об без перед передо до после за у под над о между при к с для лишь только пока едва потому оттого тому как будто словно точно на по в около из от тем тех этого здесь которой того которые ним этих собой своё свое свою него которого каждого ней своего них которых этой им ему себя мне я ты вы мы он она они оно мой твой ваш наш его её ее их свой кто что сколько какой каков чей который куда когда зачем почему этот тот такой таков эдакий оный сей экий вот все всё весь всякий сам самый каждый иной любой другой никто ничто нечего никакой ничей негде некуда никогда незачем никак некто нечто некий некоторый кто-то что-то сколько-то какой-то каков-то чей-то куда-то когда-то зачем-то почему-то кто-нибудь что-нибудь сколько-нибудь какой-нибудь чей-нибудь куда-нибудь когда-нибудь зачем-нибудь почему-нибудь что-либо кто-либо какой-либо каков-либо чей-либо куда-либо когда-либо зачем-либо почему-либо др бы ли есть очень такое можно может быть уже да нет не ни а благо более больше буквально бывает бывало было будто ведь во вовсе вон вот вроде всё всего где гляди да давай давайте даже дай дайте действительно единственно если еще знай и или именно как какое куда ладно ли лучше никак ничего нечего но однако окончательно оно поди положительно просто прямо пусть пускай разве решительно ровно самое себе скорее словно совершенно спасибо так там тебе тоже только точно хоть чего чисто что чтоб чтобы эк это а б в г д е ё ж з и й к л м н о п р с т у ф х ц ч ш щ ъ ь ы э ю я 1 2 3 4 5 6 7 8 9 0';
	//Текст собранный из базы данных
	var $_JC_text = null;
	var $_JC_metakey_text = null;
	//Цвет тегов в облаке
	var $_JC_link_color = null;
	//Включить flash или нет
	var $_JC_flash = null;
	var $_JC_flash_speed = null;
	var $_JC_flash_alt_text = null;
	var $_JC_flash_width = null;
	var $_JC_flash_height = null;
	var $_JC_flash_bgcolor = null;
	var $_JC_flash_text_color = null;
	var $_JC_flash_trans = null;
	
	
	//Конструктор класса
	function mosJVClouds($params)
	{
		$this->set_mod_param($params);
	}
	//Функция получает в качестве пааметра объект, в котором находятся значения настроек модуля, и присваивает соответствующим свойствам класса эти значения.
	function set_mod_param($params)
	{
		$this->_JC_count_tags = intval($params->get('JC_count_tags',40));
		$this->_JC_format = intval($params->get('JC_format',0));
		$this->_JC_zav = intval($params->get('JC_zav',1));
		$this->_JC_sort = intval($params->get('JC_sort',0));
		$this->_JC_text_align = $params->get('JC_text_align','justify');
		$this->_JC_rating = intval($params->get('JC_rating',1));
		$this->_JC_max_length = intval($params->get('JC_max_length',15));
		$this->_JC_min_length = intval($params->get('JC_min_length',8));
		$this->_JC_max_font = intval($params->get('JC_max_font',24));
		$this->_JC_min_font = intval($params->get('JC_min_font',10));
		$this->_JC_line_height = intval($params->get('JC_line_height',0));
		$this->_JC_limit = intval($params->get('JC_limit',0));
		$this->_JC_proc = intval($params->get('JC_proc',40));
		$this->_JC_proc_slovosoch = intval($params->get('JC_proc_slovosoch',0));
		$this->_JC_cat = $params->get('JC_cat',0);
		$this->_JC_sec = $params->get('JC_sec',0);
		$this->_JC_meta_tags = intval($params->get('JC_meta_tags',0));
		$this->_JC_meta_tags_rating = intval($params->get('JC_meta_tags_rating',0));
		$this->_JC_black_words = $params->get('JC_black_words','');
		$this->_JC_white_words = $params->get('JC_white_words','');
		$this->_JC_link_color = $params->get('JC_link_color','');
		
		$this->_JC_flash = intval( $params->get( 'JC_flash', 0 ));
		$this->_JC_flash_speed = intval( $params->get( 'JC_flash_speed', 0 ));
		$this->_JC_flash_alt_text = $params->get( 'JC_flash_alt_text', '' );
		$this->_JC_flash_width = intval( $params->get( 'JC_flash_width', 0 ));
		$this->_JC_flash_height = intval( $params->get( 'JC_flash_height', 0 ));
		$this->_JC_flash_bgcolor = $params->get( 'JC_flash_bgcolor', 'ffffff' );
		$this->_JC_flash_text_color = $params->get( 'JC_flash_text_color', '000000' );
		$this->_JC_flash_trans = intval( $params->get( 'JC_flash_trans', 1 ));
		
	}
	//Функция Возвращает строку string, в которой все русские буквенные символы переведены в нижний регистр.
	function ru_strtolower($str)
	{
		$array_up = Array("А","Б","В","Г","Д","Е","Ё","Ж","З","И","Й","К","Л","М","Н","О","П","Р","С","Т","У","Ф","Х","Ц","Ч","Ш","Щ","Ь","Ы","Ъ","Э","Ю","Я","Q","W","E","R","T","Y","U","I","O","P","A","S","D","F","G","H","J","K","L","Z","X","C","V","B","N","M");
		$array_low = Array("а","б","в","г","д","е","ё","ж","з","и","й","к","л","м","н","о","п","р","с","т","у","ф","ч","ц","ч","ш","щ","ь","ы","ъ","э","ю","я","q","w","e","r","t","y","u","i","o","p","a","s","d","f","g","h","j","k","l","z","x","c","v","b","n","m");
		$str = str_replace($array_up,$array_low,$str);
		return $str;
	}
	function en_strtolower($str)
	{
		$array_up = Array("Q","W","E","R","T","Y","U","I","O","P","A","S","D","F","G","H","J","K","L","Z","X","C","V","B","N","M");
		$array_low = Array("q","w","e","r","t","y","u","i","o","p","a","s","d","f","g","h","j","k","l","z","x","c","v","b","n","m");
		$str = str_replace($array_up,$array_low,$str);
		return $str;
	}
	//Функция заменяет в строке str знаки препинания и другие символы не относящиеся к буквам русского и латинского алфавита на пробелы, удаляет лишние пробелы и переводит все символы в нижний регистр.
	function clear_special_symbol($str,$zap)
	{
		$str = strip_tags($str);
		$str = html_entity_decode($str,ENT_QUOTES,'UTF-8');
		$str = str_ireplace('/', " ", $str);	
		$symbols = array("+","__","->","\\"," - ","{mosimage}","{mospagebreak}","\"","\'","«","»","<",">","!","(",")","$","%","?","&","^","#",":",";",".","…","*","=","`","{","}","[","]","'"," – ","—"," _"," _ ","~","•",JString::transcode(chr(160),"ASCII","UTF-8"));
		$str = str_ireplace($symbols, " ", $str);
		if ($zap)
		{
			$str = str_ireplace(",", " ", $str);
		}
		//$str = $this->en_strtolower($str);
		//$str = $this->ru_strtolower($str);
		$str = JString::strtolower($str);
		$str = JString::trim($str);
		$str = preg_replace('/\s\s+/', " ", $str);
		return $str;
	}
	//Функция осуществляет поиск слов не удовлетворяющим условиям (минимальной и максимальной длине) и удаляет их из массива key_array (Массив слов).
	//Также функция ищет похожие слова (однокоренные) - при нахождениии одно из слов уничтожается из массива key_array.
	//Ретинг другого увеличивается на 1 в массиве value_array, переданного по ссылке, и содержащего в качестве ключей слова, а значений рейтинг слова.
	//Параметр percent используется для определения уровня похожести слов.
	function sim_lev_text($key_array,&$value_array,$percent,$min_length,$max_length)
	{
		$temp_array_key = $key_array;
		for ($i=0;$i<count($temp_array_key)-1;$i++)
		{
			if ((ctype_digit($temp_array_key[$i])) || (strlen($temp_array_key[$i])<$min_length)||(strlen($temp_array_key[$i])>$max_length))
			{
				unset($key_array[$i]);
			}
			else
			{
				for ($j=$i+1;$j<count($temp_array_key);$j++)
				{
					$proc = null;
					$temp = similar_text($temp_array_key[$i],$temp_array_key[$j],$proc);
					$levin = levenshtein($temp_array_key[$i],$temp_array_key[$j],1,2,1);
					if (($proc>$percent) && ($levin<5))
					{
						$value_array[$temp_array_key[$i]]+=1;
						unset($key_array[$j]);
					}
				}
			}
		}
		return $key_array;
	}
	//Функция возвращает массив ключами которого являются словосочетания а значениями ретинг. Второй параметр определяет сколько слов будет в массиве. Сортировка призводится по уменьшению рейтинга.
	function clear_slovosoch($str,$min_length,$max_length,$count_slovosoch)
	{
		function jc_space(&$item1, $key) 
		{
			$item1 = " $item1 ";
		}
		
		$not_words = preg_replace('/\s\s+/', " ", $this->_JC_not_words);
		$array_not_words = explode(' ',$not_words);
		
		array_walk($array_not_words, 'jc_space');
		$str =str_replace($array_not_words,' ',$str); 
		
		$array_words = explode(' ',$str);
		for ($i=0;$i<count($array_words)-1;$i++)
		{
			$array_words[$i]=$array_words[$i].' '.$array_words[$i+1];
		}
		$array_words = array_count_values($array_words);
		
		$black_words = explode(',',$this->_JC_black_words);
		foreach ($black_words as $value)
		{
			if (array_key_exists(trim($value),$array_words))
			{
				unset($array_words[trim($value)]);
			}
		}
		
		arsort($array_words);
		
		$counter = 0;
		$result_array = null;
		foreach ($array_words as $key=>$value)
		{
			if ((strlen($key)<$max_length) && (strlen($key)>$min_length))
			{
				if (($counter<$count_slovosoch) )
				{
					$result_array[$key]=$value;
					$counter++;
				}	
				else
				{
					break;
				}
			}
		}
		return $result_array;
	}
	//Функция возвращает массив ключами которого являются слова а значениями ретинг. Второй параметр определяет сколько слов будет в массиве. Сортировка призводится по уменьшению рейтинга.
	 function clear_words($str,$min_length,$max_length,$count_slovosoch)
	{
		$not_words = preg_replace('/\s\s+/', " ", $this->_JC_not_words);
		$array_not_words = explode(' ',$not_words);
		
		$result_array = explode(' ',$str);	
		$result_array = array_count_values($result_array);
		
		for ($i=0; $i<count($array_not_words);$i++)
		{
			if (array_key_exists($array_not_words[$i],$result_array))
			{
				unset($result_array[$array_not_words[$i]]);
			}
		}
		
		$black_words = explode(',',$this->_JC_black_words);
		foreach ($black_words as $value)
		{
			if (array_key_exists(trim($value),$result_array))
			{
				unset($result_array[trim($value)]);
			}
		}
		
		arsort($result_array);
		$result_key_array = array_keys($result_array);
		
		$temp_array = array_slice($result_key_array,0,$count_slovosoch);
		$index_array = $count_slovosoch;
		$flag = true;
		while ($flag)
		{
			$temp_array = $this->sim_lev_text($temp_array,$result_array,$this->_JC_proc,$this->_JC_min_length,$this->_JC_max_length);
			if (count($temp_array)<$count_slovosoch)
			{	
				$temp_array = array_merge($temp_array,array_slice($result_key_array,$index_array,10));
				$index_array += 10;
			
				if ($index_array>count($result_key_array))
				{
					$temp_array = $this->sim_lev_text($temp_array,$result_array,$this->_JC_proc,$this->_JC_min_length,$this->_JC_max_length);
					$flag=false;
				}
			}
			else
			{
				$flag = false;
			}
		}
		
		$resultat= null;
		$counter = 1;
		foreach ($temp_array as $key)
		{
			if ($counter<=$count_slovosoch)
			{
				$resultat[$key]=$result_array[$key];
			}
			$counter++;
		}
		
		return $resultat;
	}
	//Функция получает данные из базы в зависимости от свойства _JC_meta_tags. Присваивает результат свойству _JC_text. Либо текст содержащийся в ключевых словах либо текст сайта.
	function db_full_text()
	{
		$database = & JFactory::getDBO();
		$post_query = null;
		$limit_query = null;
		
		if($this->_JC_cat)
		{	
			$post_query = $post_query.' AND catid IN ('.$this->_JC_cat.') ';
		}
		if($this->_JC_sec)
		{	
			$post_query = $post_query.' AND sectionid IN ('.$this->_JC_sec.') ';
		}
		if ($this->_JC_limit)
		{
			$limit_query = " LIMIT 0,".$this->_JC_limit." ";
		}
		
		
		if (($this->_JC_meta_tags_rating==1) || (($this->_JC_meta_tags==0)))
		{
			$query = "SELECT `title` , `introtext` , `fulltext` FROM #__content WHERE state = 1 ".$post_query.$limit_query; 
		
			$database->setQuery($query);
			$result_get_query = $database->loadObjectList();
		
			$full_text = "";
			foreach($result_get_query as $obj)
			{
				$full_text .= $obj->title." ".$obj->introtext." ".$obj->fulltext." ";
			}
			
			$this->_JC_text = $this->clear_special_symbol($full_text,1);
		}	
		
		if ($this->_JC_meta_tags==1)
		{
			$query_meta = "SELECT `metakey` FROM #__content WHERE state = 1 ".$post_query.$limit_query; 
			
			$database->setQuery($query_meta);
			$result_get_query = $database->loadObjectList();
		
			$meta_text = "";
			foreach($result_get_query as $obj)
			{
				$meta_text .= $obj->metakey.",";
			}
			$this->_JC_metakey_text = $this->clear_special_symbol($meta_text,0);
			$point = array(" , ",", "," ,");
			$this->_JC_metakey_text = str_replace($point,",",$this->_JC_metakey_text);
			//echo($this->_JC_metakey_text);
		}	
	}
	//Формирование массива определенной размерности из ключевых слов заданных в материалах сайта.
	function add_metakey_array($str,$full_text,$min_length,$max_length,$count)
	{
		$temp_array = explode(",",$str);
		$metakey_array = array_count_values($temp_array);
		
		if ($this->_JC_meta_tags_rating==1)
		{
			$full_array = explode(" ",$full_text);
		
			$words_array = array_count_values($full_array);
		
			for ($i=0;$i<count($full_array)-1;$i++)
			{
				$slovosoch_array[$i]=$full_array[$i].' '.$full_array[$i+1];
			}
		
			$slovosoch_array = array_count_values($slovosoch_array);
			$merge_array = array_merge($words_array,$slovosoch_array);
		
			foreach ($temp_array as $k=>$v)
			{
				if ((strlen($v)>=$min_length) && (strlen($v)<=$max_length))
				{
					if(array_key_exists($v,$merge_array))
					{
						$stat_array[$v]=$merge_array[$v];
					}
					else
					{
						$stat_array[$v]=$metakey_array[$v];
					}
				}
			}
		}
		else
		{
			$stat_array = $metakey_array;
		}
		
		arsort($stat_array);
		
		$counter = 0;
		foreach ($stat_array as $k=>$v)
		{
			if ($counter<$count)
			{
				$result_array[$k]=$v;
				$counter++;
			}
			else
			{
				break;
			}
		}
		
		
		return $result_array;
	}
	//Неаосредственно формирование облака
	function show_cloud()
	{
		$this->db_full_text();
		
		$white_words_result = null;
		$white_words_array = null;
		if ($this->_JC_white_words!='')
		{
			$white_words_array = explode(',',$this->_JC_white_words);
			foreach ($white_words_array as $word)
			{
				$temp = explode('=',trim($word));
				$white_words_result[$temp[0]]=$temp[1];
			}
		}
		
		
		if ($this->_JC_meta_tags==1)
		{
			$count_all = $this->_JC_count_tags - count($white_words_result);
			$array_cloud = $this->add_metakey_array($this->_JC_metakey_text,$this->_JC_text,$this->_JC_min_length,$this->_JC_max_length,$count_all);
			if (count($white_words_result)!=0)
			{
				$array_cloud = array_merge($array_cloud,$white_words_result);
			}	
		}
		else
		{
			$one=null;$two=null;
			if ($this->_JC_proc_slovosoch==0)
			{
				$count_all = $this->_JC_count_tags - count($white_words_result);
				$two = $this->clear_words($this->_JC_text,$this->_JC_min_length,$this->_JC_max_length,$count_all);
				if (count($white_words_result)!=0)
				{
					$two = array_merge($two,$white_words_result);
				}
			}
			elseif ($this->_JC_proc_slovosoch==100)
			{
				$count_all = $this->_JC_count_tags - count($white_words_result);
				$one = $this->clear_slovosoch($this->_JC_text,$this->_JC_min_length,$this->_JC_max_length,$count_all);
				if (count($white_words_result)!=0)
				{
					$one = array_merge($one,$white_words_result);
				}
			}
			else
			{
				$count_all = $this->_JC_count_tags - count($white_words_result);
				$count_slovosoch = round(($count_all/100)*$this->_JC_proc_slovosoch);
				$count_words = $this->_JC_count_tags - $count_slovosoch;
				$one = $this->clear_slovosoch($this->_JC_text,$this->_JC_min_length,$this->_JC_max_length,$count_slovosoch);
				$two = $this->clear_words($this->_JC_text,$this->_JC_min_length,$this->_JC_max_length,$count_words);
			}
			
			if ((count($one)!=0)&&(count($two)!=0))
			{
				$max_rating_words = max($two);
				$max_rating_slovosoch = max($one);
				$kof = round((($max_rating_words/$max_rating_slovosoch)/100)*$this->_JC_proc_slovosoch);
				foreach ($one as $k=>$v)
				{
					$one[$k]=$v*$kof;
				}
				$array_cloud = array_merge($one,$two);
				if (count($white_words_result)!=0)
				{
					$array_cloud = array_merge($array_cloud,$white_words_result);
				}
			}
			else
			{
				if (count($one)!=0)
				{
					$array_cloud = $one;
				}
				if (count($two)!=0)
				{
					$array_cloud = $two;
				}
			}
			
			
		}
		
		if (count($array_cloud)>3)
		{
			switch ($this->_JC_sort)
				{
					case 0:
						ksort($array_cloud);
						break;
					case 1:
						krsort($array_cloud);
						break;
					case 2:
						asort($array_cloud);
						break;
					case 3:
						arsort($array_cloud);
						break;
					case 4:
						$temp_array = $array_cloud;
						$temp_array_key = null;
						foreach ($temp_array as $k=>$v)
						{
							$temp_array_key[] = $k;
						}
						shuffle($temp_array_key);
						$array_cloud = null;
						foreach ($temp_array_key as $k=>$v)
						{
							$array_cloud[$v] = $temp_array[$v];
						}
						break;	
				}
				
				$font_range = $this->_JC_max_font - $this->_JC_min_font;
				$minimum_count = min($array_cloud); 
				$maximum_count = max($array_cloud); 
				$myrange = $maximum_count - $minimum_count;		

				
				$min_count = log($minimum_count+1);
				$max_count = log($maximum_count+1);
				$count_range = $max_count-$min_count;
				
				echo("<p align=\"".$this->_JC_text_align."\">");
				$tags = '';
				foreach($array_cloud as $k=>$v)
				{
					if (($myrange==0)||($count_range==0))	
					{
						$size = round(($this->_JC_min_font + $this->_JC_max_font)/2);
					}
					else
					{
						if ($this->_JC_zav==0)
						{
							$size = $this->_JC_min_font+($font_range*(($v-$minimum_count)/$myrange));
						}
						else
						{
							$size=$this->_JC_min_font+((log($v+1)-$min_count)*$font_range/$count_range);
						}	
					}	
				
					$title_rate = "(".$v.")";
				
					$rate = "";
					if ($this->_JC_rating==0)
					{
						$rate = "(".$v.")";
					}
				
					$listing = " ";
					if ($this->_JC_format==1)
					{
						$listing = "<br>";
					}
				
					$line_height = "";
					if ($this->_JC_line_height<>"")
					{
						$line_height = "line-height: ".$JC_line_height."px;";
					}
					//sefRelToAbs
					$href = "index.php?option=com_search&amp;searchword=$k&amp;searchphrase=all&amp;ordering=newest";
					
															
					
					if ($this->_JC_flash==0)
					{
						if ($this->_JC_link_color=='')
						{
							$tags .= "<a href=\"$href\" title=\"".$k.$title_rate."\" style=\"font-size: ".$size."px; ".$line_height." \">".$k.$rate."</a>".$listing;
						}
						else
						{
							$tags .= "<a href=\"$href\" title=\"".$k.$title_rate."\" style=\"font-size: ".$size."px; ".$line_height." color: ".$this->_JC_link_color."; \">".$k.$rate."</a>".$listing;
						}
					}
					else
					{
						$f=$k;
						$f= iconv('cp1251','utf-8//IGNORE',$f);
						if ($this->_JC_link_color=='')
						{
							$tags .= "<a href='index.php?option=com_search%26amp;searchword=$f"."' style='font-size:".$size."px;'>".$k."</a> ";
						}
						else
						{
							$tags .= "<a href='index.php?option=com_search%26amp;searchword=$f"."' style='font-size:".$size."px;'>".$k."</a> ";
						}
					}
					
				}
	


				if ($this->_JC_flash==1)
				{
	
					$text_color = $this->_JC_flash_text_color;
					$alt_test = $this->_JC_flash_alt_text;
					$width = $this->_JC_flash_width;
					$height = $this->_JC_flash_height;
					$bgcolor = $this->_JC_flash_bgcolor;
					$speed = $this->_JC_flash_speed;
					$trans =$this->_JC_flash_trans;
					
					?>
		
					<table width=100% height=100% border=0>
						<tr valign=middle>
							<td align=center>
							<script type="text/javascript" src="<?php echo JURI::base();?>modules/mod_jvclouds3D/jvclouds3D/swfobject.js"></script>
							<div id="wpcumuluswidgetcontent"><?php echo $alt_text;?></div>
							<script type="text/javascript">
								var rnumber = Math.floor(Math.random()*9999999);
								var widget_so = new SWFObject("<?php echo JURI::base();?>modules/mod_jvclouds3D/jvclouds3D/tagcloud.swf?r="+rnumber, "tagcloudflash", "<?php echo $width;?>", "<?php echo $height;?>", "9", "#<?php echo $bgcolor;?>");
								<?php 
									if( $trans == '1' ){echo'widget_so.addParam("wmode", "transparent");';}
								?>
								widget_so.addParam("allowScriptAccess", "always");
								widget_so.addVariable("tcolor", "0x<?php echo $text_color;?>");
								widget_so.addVariable("tspeed", "<?php echo $speed;?>");
								widget_so.addVariable("distr", "true");
								widget_so.addVariable("mode", "tags");
								widget_so.addVariable("tagcloud", "<span><?php echo $tags;?></span>");
								widget_so.write("wpcumuluswidgetcontent");
							</script>
							</td>
						</tr>
					</table>
					<?php
				}
				else
				{
					echo $tags;
				}
		}		
	}
}

?>
