<?php
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

jimport('joomla.application.component.model');

class TranslatorModelCron extends JModel
{
	var $_notran	=	array();
	
	function __construct()
	{
		parent::__construct();		
	}
	
	function cron(){
		global $mainframe, $option;
		
		$db	= & JFactory::getDBO();		
		
		
		$croncode = JRequest::getVar('croncode', '' );
			
		$param = JComponentHelper::getParams($option);
		//var_dump($param->get( 'croncode'));die();
		
		if ($param->get( 'croncode') <> $croncode){			
			return ;
		}
		
		$query	=	"SELECT * FROM #__languages";
		$db->setQuery($query);
		$rows	=	$db->loadObjectList();
		
		$default_language = &$this->getDefault();
		
		
		$trans = new TranslatorCron();	
		$count=0;
		foreach ($rows as $row){
			if (strtolower($row->shortcode) <> strtolower($default_language)){
				$results = $this->getListarticle($row->id);
				
				foreach ($results as $result ){		
					if ($count > 109) {
						return ;
					}
					$a = $trans->save($result->id, $default_language, $row->id );
					if($count> 99) break;
					$count++;			
				}
			}
			//echo '<br />';
		}	
		echo 'Ok';
		die();
	}

	function getDefault() {
		global $mainframe;
		$db	=	&JFactory::getDBO();
		$client	=& JApplicationHelper::getClientInfo(JRequest::getVar('client', '0', '', 'int'));		
		$params	=	&JComponentHelper::getParams('com_languages');
		$default_language	=	$params->get($client->name, 'en-GB');
		$query				=	"SELECT code_translator FROM #__yos_translator WHERE code_language = '$default_language'";
		$db->setQuery($query);
		$code_translator	=	$db->loadResult();
		if (!$code_translator) {
			$mainframe->redirect('index.php?option=com_yos_translator&view=contents','not config code translator');
		}
		
		return $code_translator;
	}
	
	function  getListarticle($language_id){
		global $mainframe;
		$db =& $this->getDBO();
		$query = "SELECT JC.`id`, ( SELECT JC.`modified` < JFC1.`modified` ) AS 'status', 1 AS 'total' 
			FROM `#__content` AS JC LEFT JOIN `#__jf_content` AS JFC1 ON JC.id = JFC1.reference_id AND 
			JFC1.reference_field = 'title' AND JFC1.reference_table = 'content' AND JFC1.language_id=".$language_id." 
			LEFT JOIN `#__languages` AS JL ON JFC1.language_id = JL.id LEFT JOIN `#__users` AS JU ON JFC1.modified_by = JU.id 
			WHERE JC.`state` >= 0 HAVING `status` IS NULL OR `status` = 0 ";
		$db->setQuery($query);
		$rows	=	$db->loadObjectList();
		//var_dump($rows); die();
		return $rows;
	}
}
class TranslatorCron{
	
	var $_notran	=	array();
	
	function __construct()
	{
		
	}
	function notranslate($data){
		
		$regex	=	'/\{notranslate\}(.*?)\{\/notranslate\}/m';
		preg_match_all($regex, $data, $matches);
		$f_number	=	count($matches[0]);
		
		if (!$f_number) {
			return $data;
		}		
		
		for ($i =0 ; $i<$f_number; $i++){
			$data	=	str_replace($matches[0][$i], md5($matches[0][$i]),$data);
			$this->_notran[]	=	array('hash'=> md5($matches[0][$i]), 'value'=> $matches[1][$i]);
		}		
		
		return $data;
	}


	function save($id, $src, $dst ){
		global $mainframe;		
		$db	=	&JFactory::getDBO();
		
		//$id	=	JRequest::getInt('id');		
		//$src=	JRequest::getString('src');
		//$dst=	JRequest::getInt('dst');
		//$published	=	JRequest::getString('published');
		$published	= 1;	//$published == 'true' ? 1 : 0;
		$lang_id	=	$dst;

		//$lang	=	&JTable::getInstance('languages');
		$query = "SELECT * FROM #__languages WHERE id = $dst";
		$db->setQuery($query);
		$lang= $db->loadObject();
		//$lang->load($dst);
		
		
		$query	=	"SELECT code_translator FROM #__yos_translator WHERE code_language = '$lang->code'";
		$db->setQuery($query); 
		$dst	=	$db->loadResult();
		
		if (!$dst) {
			echo 'Error!';
			die();
		}
				
		$query = "SELECT * FROM #__content WHERE id = $id";
		$db->setQuery($query);
		$content= $db->loadObject();
		
		//build a page to translate
		$pageContent = '';
		
		$titleHashBegin = md5('JLORD_BEGIN_TITLE_AREA');
		$titleHashEnd = md5('JLORD_END_TITLE_AREA');
		$pageContent .= '' . $titleHashBegin . '<div>' . $content->title . '</div>' . $titleHashEnd . '';
		$pageContent .= "<br />";
		
		$introtextHashBegin = md5('JLORD_BEGIN_INTROTEXT_AREA');
		$introtextHashEnd = md5('JLORD_END_INTROTEXT_AREA');
		$pageContent .= '' . $introtextHashBegin . '<div>' . $content->introtext . '</div>' . $introtextHashEnd . '';
		$pageContent .= "<br />";
		
		$fulltextHashBegin = md5('JLORD_BEGIN_FULLTEXT_AREA');
		$fulltextHashEnd = md5('JLORD_END_FULLTEXT_AREA');
		$pageContent .= '' . $fulltextHashBegin . '<div>' . $content->fulltext . '</div>' . $fulltextHashEnd . '';
		$pageContent .= "<br />";
		
		$metakeyHashBegin = md5('JLORD_BEGIN_METAKEY_AREA');
		$metakeyHashEnd = md5('JLORD_END_METAKEY_AREA');
		$pageContent .= '' . $metakeyHashBegin . '<div>' . $content->metakey . '</div>' . $metakeyHashEnd . '';
		$pageContent .= "<br />";
		
		$metadescHashBegin = md5('JLORD_BEGIN_METADESC_AREA');
		$metadescHashEnd = md5('JLORD_END_METADESC_AREA');
		$pageContent .= '' . $metadescHashBegin . '<div>' . $content->metadesc . '</div>' . $metadescHashEnd . '';
		$pageContent .= "<br />";
		
		//No translate
		$pageContent	=	$this->notranslate($pageContent);
		$pageContent	=	$this->personconfig($pageContent);
				
		
		//require helper
		
		//require_once($mosConfig_absolute_path.'/administrator/components/com_yos_translator/admin/helpers'.DS.'translator.php');
		require_once(JPATH_COMPONENT.DS.'translator.php');
		//var_dump(JPATH_COMPONENT.DS);
		$translator = new YOS_translator();
		$languagePair	=	$src.'|'.$dst;	
		
		
		$tsl_pageContent = $translator->translate($languagePair, $pageContent);		
		
		//Prepare save content
		if (count($this->_notran)) {
			foreach ($this->_notran as $notran) {
				$tsl_pageContent	=	str_replace($notran['hash'], $notran['value'], $tsl_pageContent);
				$tsl_pageContent	=	str_replace('<div> '.$notran['value'].' </div>', $notran['value'], $tsl_pageContent);
			}
		}
		
		//remove old translated records
		$query	=	"DELETE FROM #__jf_content WHERE reference_id = $id AND language_id = $lang_id AND reference_table= 'content'";
		$db->setQuery($query);
		$db->query();
		
		//get table
		//JTable::addIncludePath(JPATH_COMPONENT_ADMINISTRATOR.DS.'tables');
		//$tbl_jf_content =& JTable::getInstance('jf_content', 'Table');
		
		//get title:
		if (preg_match('/' . $titleHashBegin . '\s*<div>(.*?)<\/div>\s*' . $titleHashEnd . '/is', $tsl_pageContent, $match)) {
			$this->store_jf($lang_id, $id, 'content', 'title', trim($match[1]), md5($content->title), $content->title, $published);
		}
		
		//get introtext:
		if (preg_match('/' . $introtextHashBegin . '\s*<div>(.*?)<\/div>\s*' . $introtextHashEnd . '/is', $tsl_pageContent, $match)) {
			$this->store_jf($lang_id, $id, 'content', 'introtext', trim($match[1]), md5($content->introtext), $content->introtext, $published);
		}
		
		//get fulltext:
		if (preg_match('/' . $fulltextHashBegin . '\s*<div>(.*?)<\/div>\s*' . $fulltextHashEnd . '/is', $tsl_pageContent, $match)) {
			$this->store_jf($lang_id, $id, 'content', 'fulltext', trim($match[1]), md5($content->fulltext), $content->fulltext, $published);
		}
		
		//get metakey:
		if (preg_match('/' . $metakeyHashBegin . '\s*<div>(.*?)<\/div>\s*' . $metakeyHashEnd . '/is', $tsl_pageContent, $match)) {
			$this->store_jf($lang_id, $id, 'content', 'metakey', trim($match[1]), md5($content->metakey), $content->metakey, $published);
		}
		
		//get metadesk:
		if (preg_match('/' . $metadescHashBegin . '\s*<div>(.*?)<\/div>\s*' . $metadescHashEnd . '/is', $tsl_pageContent, $match)) {
			$this->store_jf($lang_id, $id, 'content', 'metadesc', trim($match[1]), md5($content->metadesc), $content->metadesc, $published);
		}
		
		
		//copy alias, images, publish_up, publish_down, atribs fields
		$this->store_jf($lang_id, $id, 'content', 'title_alias', $content->title_alias, md5($content->title_alias), $content->title_alias, $published);
		
		$this->store_jf($lang_id, $id, 'content', 'images', $content->images, md5($content->images), $content->images, $published);
		
		$this->store_jf($lang_id, $id, 'content', 'publish_up', $content->publish_up, md5($content->publish_up), $content->publish_up, $published);
		
		$this->store_jf($lang_id, $id, 'content', 'publish_down', $content->publish_down, md5($content->publish_down), $content->publish_down, $published);
		
		$this->store_jf($lang_id, $id, 'content', 'attribs', $content->attribs, md5($content->attribs), $content->attribs, $published);
		
		return 'ok';
	}
	
	function personconfig( $data ){
			$config =& JComponentHelper::getParams('com_yos_translator');
		$param_notranslate= $config->get('notranslate');
		$param_notranslate=	preg_replace('/\s/','',$param_notranslate);
		if ($param_notranslate) {
			$arr_tag	=	explode(';',$param_notranslate);			
			foreach ($arr_tag as $tag) {
				if ($tag) {
					$arr_sp		=	explode(',', $tag);
					$tagfirst	=	$arr_sp[0];
					$taglast	=	$arr_sp[1];
					if (!($tagfirst && $taglast)) {
						continue;
					}
					
					preg_match_all('/'.str_replace('/','\/',preg_quote($tagfirst)).'(.*?)'.str_replace('/','\/',preg_quote($taglast)).'/', $data, $matches);
					$f_number	=	count($matches[0]);
					if (!$f_number) {
						continue;
					}
					
					for ($i =0 ; $i<$f_number; $i++){
						//$data	=	str_replace($matches[0][$i], md5($matches[0][$i]),$data);
						$data	=	str_replace($matches[0][$i], '<div>'.md5($matches[0][$i]).'</div>',$data);
						$this->_notran[]	=	array('hash'=> md5($matches[0][$i]), 'value'=> $matches[0][$i]);
					}
					
				}
			}			
		}
		
		return $data;
	}

	function store_jf($lang_id, $reference_id, $reference_table, $reference_field , $value,$original_value,$original_text, $published){
		
		$user	= & JFactory::getUser();
		$db	=	&JFactory::getDBO();
		
		$modified = date( 'Y-m-d H:i:s' );
		
		$modified_by = $user->get('id');
				
		//store new translate value
		$query = "INSERT INTO #__jf_content SET
			`id` = null,
			`language_id` = $lang_id,
			`reference_id` = $reference_id,		
			`reference_table` = ". $db->Quote($reference_table) .",
			`reference_field` = ". $db->Quote($reference_field) .",
			`value` = ". $db->Quote($value) .",
			`original_value` = ". $db->Quote($original_value) .",
			`original_text` = ". $db->Quote($original_text) .",
			`modified` = ". $db->Quote($modified) .",
			`modified_by` = $modified_by,
			`published` = $published
		";
		$db->setQuery($query);
		$db->query();
	}
}

?>